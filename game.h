#ifndef _GAME_H
#define _GAME_H

#include <string>
#include <memory>
#include <vector>


enum class Colour;
enum class GameStatus;
enum class ChessPieceType;
class Player;
class Move;
class Board;
class Spot;
class ChessPiece;
// determine compilation dependencies 

class Game {
	std::shared_ptr<Board> board;
	std::vector<std::shared_ptr<Move>> moves;
	std::shared_ptr<Player> currentPlayer;
	std::vector<std::shared_ptr<Player>> players;
	GameStatus status;
	Colour firstTurn;

	public:
	Game();
	std::shared_ptr<Board> getBoard() const;
	std::shared_ptr<Player> getCurrentPlayer() const;
	std::shared_ptr<Player> getPlayer(std::string colour) const;
	std::shared_ptr<Move> getPreviousMove() const;
	GameStatus getStatus() const;
	void undoMove(); // will change current player
	void resign();
	void addPlayer(bool isHuman, Colour playerColour, int level = 0);
	void addPiece(int row, int col, char piece);
	void removePiece(int row, int col);
	void changeTurn(std::string turn);
	bool isActive() const;
	bool setupDone() const;
	void startGame(std::string whitePlayer, std::string blackPlayer);
	bool isFinished() const;
	void setupMode();
	void playerMove(int startRow = -1, int startCol = -1, int endRow = -1, int endCol = -1, char promoteChar = '\0');
	bool isCheck(Colour colour);
	bool isCheckmate(Colour colour);
	bool isStalemate(Colour colour);

	private:
	void createEmptyBoard();
	Colour convertToColour(std::string colour) const;
	bool isValidCharOfPromotionPiece(char ch);
	bool normalMoveCheck(std::shared_ptr<ChessPiece> chessMoved, std::shared_ptr<Spot> startSpot, std::shared_ptr<Spot> endSpot);
	bool myKingDanger();
	bool stillMoved(std::shared_ptr<ChessPiece> lastChess, std::vector<std::shared_ptr<Move>> moves);
	std::shared_ptr<Player> findOpponent();
	bool isMyKingCheck(std::shared_ptr<Spot> kingSpot, std::shared_ptr<Player> opponent);
	// no change side happen
	void undoPlayerMove();
};

#endif

