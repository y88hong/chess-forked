#include "player.h"
#include "chessPiece.h"

Player::Player(std::vector<std::shared_ptr<ChessPiece>>& allPieces, bool castled,
    bool ishuman, Colour side) : pieces{ allPieces }, hasCastle{ castled },
    isHuman{ ishuman }, side{ side } {
    if (king == nullptr) {
        for (auto p : allPieces) {
            if (p->getPieceType() == ChessPieceType::KING) {
                king = p;
            }
        }
    }
}

bool Player::getIsHuman() {
    return isHuman;
}

Colour Player::getSide() {
    return side;
}

bool Player::getHasCastle() {
    return hasCastle;
}

void Player::setHasCastle(bool b) {
    hasCastle = b;
}
void Player::addPlayerPiece(std::shared_ptr<ChessPiece> c) {
    pieces.push_back(c);
    if (c->getPieceType() == ChessPieceType::KING) {
        king = c;
    }
}

std::vector<std::shared_ptr<ChessPiece>> &Player::getPlayerPieces() {
    return pieces;
}
std::shared_ptr<ChessPiece> Player::getKing() {
    return king;
}

