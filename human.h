#ifndef _HUMAN_H_
#define _HUMAN_H_
#include "player.h"
#include <vector>


class Human : public Player {
public:
    Human(std::vector<std::shared_ptr<ChessPiece>> &allChess, bool castle, bool human, Colour side);
};

#endif

