#ifndef __ROOK__H
#define __ROOK__H

#include "chessPiece.h"
#include "chessPieceType.h"
#include "colour.h"

class Spot;
class Board;

class Rook: public ChessPiece {
    public:
    Rook(ChessPieceType name, Colour colour, int row, int col);
    Rook(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col);
    
    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck = true) override;
    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck = true) override;
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, Board *board) override;
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) override;
};

#endif

