#ifndef _TEXTOBSERVER_H_
#define _TEXTOBSERVER_H_

#include "observer.h"
#include <memory>
#include <vector>

class DisplayBoard;

class TextObserver : public Observer
{
    std::shared_ptr<DisplayBoard> subject;

public:
    TextObserver(std::shared_ptr<DisplayBoard> sub);
    ~TextObserver();
    void notify();
};

#endif

