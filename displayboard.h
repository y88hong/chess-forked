#ifndef _DISPLAYBOARD_H_
#define _DISPLAYBOARD_H_

#include "subject.h"
#include <memory>

class Board;

class DisplayBoard : public Subject {
    std::shared_ptr<Board> board;

public:
    DisplayBoard(std::shared_ptr<Board> b);
    std::shared_ptr<Board> getPrintBoard();
};
#endif

