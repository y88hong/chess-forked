#ifndef _SPOT_H
#define _SPOT_H
#include <iostream>
#include <memory>
#include "colour.h"

class ChessPiece;

class Spot {
	const int row;
	const int col;
	const Colour spotColour;
	std::shared_ptr<ChessPiece> piece;

	public:
	Spot(int row, int col, Colour spotColour, std::shared_ptr<ChessPiece> piece = nullptr);
	int getRow() const;
	int getCol() const;
	Colour getSpotColour() const;
	std::shared_ptr<ChessPiece> getPiece() const;
	void setPiece(std::shared_ptr<ChessPiece> piece);
	std::shared_ptr<ChessPiece> removePiece();
	bool isOccupied() const;
	friend std::ostream &operator<<(std::ostream &out, const Spot &spot);
};

#endif


