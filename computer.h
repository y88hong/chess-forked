#ifndef _COMPUTER_H_
#define _COMPUTER_H_
#include <vector>
#include "player.h"

class Board;
class ChessPiece;
class Strategy;
class Move;
class Computer : public Player
{
    int level;
    std::shared_ptr<Strategy> algorithm;

public:
    Computer(int l, std::vector<std::shared_ptr<ChessPiece>>& allChess, bool castle, bool human, Colour side);
    // return index of moves that can capture opponent's piece
    std::vector<int> computerMove(std::shared_ptr<Board> board, Colour color, std::vector<std::shared_ptr<Move>> allMoves);
};
#endif

