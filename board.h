#ifndef _BOARD_H
#define _BOARD_H
#include <vector>
#include <iostream>
#include <memory>
// determine compilation dependencies
class Spot;
enum class Colour;
enum class ChessPieceType;
class ChessPiece;

class Board {
	std::vector<std::vector<std::shared_ptr<Spot>>> spots; // stores all the spots on the board
	std::vector<std::shared_ptr<ChessPiece>> whitePieces; // stores all the white pieces on the board
	std::vector<std::shared_ptr<ChessPiece>> blackPieces; // stored all the black pieces on the board
	std::shared_ptr<ChessPiece> whiteKing; // stores the white king on the board
	std::shared_ptr<ChessPiece> blackKing; // stored the black king on the board
	const int width; // width of the board
	const int height; // height of the board

	public:
	Board(int width = 8, int height = 8, bool custom = false);
	bool isValidCoordinate(int row, int col) const;
	std::shared_ptr<Spot> getSpot(int row, int col) const;
	void addPiece(int row, int col, char piece);
	void removePiece(int row, int col);
	bool done();
	int getWidth() const;
	int getHeight() const;
	std::vector<std::shared_ptr<ChessPiece>> &getWhitePiece();
     	std::vector<std::shared_ptr<ChessPiece>> &getBlackPiece();
	std::shared_ptr<ChessPiece> &getBlackKing();
	std::shared_ptr<ChessPiece> &getWhiteKing();
	friend std::ostream &operator<<(std::ostream &out, const Board &board);
	void addDefaultPieces();
	bool isEmpty();
	bool isCheck(Colour colour);
	bool isCheck(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Colour colour);
	bool isCheckmate(Colour colour);
	bool isStalemate(Colour colour);

	private:
	bool findWhiteKing();
	bool findBlackKing();
	bool noPawns();
	std::shared_ptr<ChessPiece> createChessPiece(ChessPieceType pieceType, Colour colour, int row, int col);
	bool validPiece(char piece);
	ChessPieceType charToPieceType(char piece);
	Colour findColour(char piece);
	void createEmptyBoard();
};

#endif 


