#include "beginner.h"
#include "board.h"
#include "chessPiece.h"
#include <algorithm>
#include <random>

// calculateMove(board, colour, allMoves) basic level computer move algorithm
//   * calls the base class implementation, a random valid move generator
std::vector<int> Beginner::calculateMove(std::shared_ptr<Board> board, Colour colour,std::vector<std::shared_ptr<Move>> allMoves) {
    return Strategy::calculateMove(board, colour, allMoves);
}
