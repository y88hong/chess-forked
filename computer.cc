#include "computer.h"
#include <memory>
#include <vector>
#include "beginner.h"
#include "intermediate.h"
#include "move.h"
#include "board.h"
#include "expert.h"

Computer::Computer(int l, std::vector<std::shared_ptr<ChessPiece>>& allChess, bool castle, bool human, Colour side) : Player(allChess, castle, human, side), level{ l }
{

    switch (level) {
    case 1:
        algorithm = std::make_shared<Beginner>();
        break;
    case 2:
        algorithm = std::make_shared<Intermediate>();
        break;
    case 3:
        algorithm = std::make_shared<Expert>();
        break;
    }
}

std::vector<int> Computer::computerMove(std::shared_ptr<Board> board, Colour color, std::vector<std::shared_ptr<Move>> allMoves) {
    return algorithm->calculateMove(board, getSide(), allMoves);
}
