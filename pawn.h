#ifndef __PAWN__H
#define __PAWN__H

#include "chessPiece.h"
#include "chessPieceType.h"
#include "colour.h"

class Spot;
class Board;

class Pawn: public ChessPiece {
    public:
    Pawn(ChessPieceType name, Colour colour, int row, int col);
    Pawn(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col);

    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck = true) override; 
    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck = true) override; 
    bool validMove(bool isEnPassantMove, std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck = true);
    bool validMove(bool isEnPassantMove, std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck = true);
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, Board *board) override;
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) override;

    private:
    bool isValidPawnCapture(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, bool isEnPassantMove);
    char getRandomPieceAsChar() const;
};

#endif


