#ifndef _PLAYER_H
#define _PLAYER_H

#include <vector>
#include <memory>
#include "colour.h"

class ChessPiece;

class Player
{
    std::vector<std::shared_ptr<ChessPiece>>& pieces;
    bool hasCastle;
    bool isHuman;
    Colour side;
    std::shared_ptr<ChessPiece> king; 

public:
    Player(std::vector<std::shared_ptr<ChessPiece>>& allPieces, bool castled = false,
        bool ishuman = true, Colour side = Colour::WHITE);
    bool getIsHuman();
    Colour getSide();
    bool getHasCastle();
    void setHasCastle(bool b);
    std::shared_ptr<ChessPiece> getKing();
    void addPlayerPiece(std::shared_ptr<ChessPiece> c);
    std::vector<std::shared_ptr<ChessPiece>> &getPlayerPieces();
};

#endif

