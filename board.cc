#include "board.h"
#include <vector>
#include <iostream>
#include <memory>
#include "spot.h"
#include "pawn.h"
#include "king.h"
#include "queen.h"
#include "bishop.h"
#include "rook.h"
#include "knight.h"
#include "chessPieceType.h"
#include "colour.h"
#include "chessPiece.h"

/**
 * Board(int wodth, int height, bool custom) creates a board of width x height
 * Note: If custom is true, then creates a custom board (empty board), else 
 * 	creates a default board with default pieces
 */
Board::Board(int width, int height, bool custom): width{width}, height{height} {
	// creating an empty board 
	// for custom setup
	this->createEmptyBoard();
	// creating default board
	if (custom == false) {
		this->addDefaultPieces();
	}
}

/**
 * isEmpty() returns if the board is empty
 */
bool Board::isEmpty() {
	return this->blackPieces.size() == 0 && this->whitePieces.size() == 0;
}

/**
 * addPiece(int row, int col, char piece) adds piece to the given 
 * 	row and col
 * Note: if a piece is already there in a particular row and col
 * 	then it is replaced
 */
void Board::addPiece(int row, int col, char piece) {
	// checks if valid piece and co-ordinate
	if (this->isValidCoordinate(row, col) && this->validPiece(piece)) {
		ChessPieceType pieceType = this->charToPieceType(piece);
		Colour colour = this->findColour(piece);
		// if a piece is in the spot, then it is removed
      		if (this->getSpot(row, col)->getPiece() != nullptr) {
			this->removePiece(row, col);
		}
		std::shared_ptr<ChessPiece> chessPiece = this->createChessPiece(pieceType, colour, row, col);
		this->getSpot(row, col)->setPiece(chessPiece);
		if (colour == Colour::WHITE) {
			whitePieces.push_back(chessPiece);
		} else if (colour == Colour::BLACK) {
			blackPieces.push_back(chessPiece);
		}
		std::cout << "Successfully added " << piece << "." << std::endl;
	} else {
		std::logic_error("Invalid addition of piece to board. Check row and col the piece is to be added or the type of piece.");
	}
}

/**
 * removePiece(row, col) removes a piece from the board.
 * Note: if there is no piece in the given row and col, then no effect.
 */
void Board::removePiece(int row, int col) {
	if (this->isValidCoordinate(row, col)) {
		auto piece = this->getSpot(row, col)->removePiece();
		if (piece) piece->setIsValidPiece(false);
	} else {
		std::logic_error("Piece could not be removed.");
	}
}

/**
 * done() checks if the conditions for exiting the setup mode is satisfied.
 * Note: The conditions checked are:
 * 	 1. Exactly one white and black king are on the board
 * 	 2. There are no pawns in the first and last row of the board.
 * 	 3. The white and black king are not in check
 */
bool Board::done() {
	bool done = false;
	if (this->findWhiteKing() && this->findBlackKing() && this->noPawns()) {
		done = true;
	}
	// remove all dead pieces
	// for black pieces 
	for (std::size_t black = 0; black < this->blackPieces.size(); black++) {
		if (this->blackPieces.at(black)->getIsValidPiece() == false) {
			blackPieces.erase(blackPieces.begin() + black);
		}
	}
	// for white pieces
	for (std::size_t white = 0; white < this->whitePieces.size(); white++) {
		if (this->whitePieces.at(white)->getIsValidPiece() == false) {
			whitePieces.erase(whitePieces.begin() + white);
		}
	}
	// find the black king
	for (auto piece: this->blackPieces) {
		if (piece != nullptr) {
			if (piece->getPieceType() == ChessPieceType::KING && piece->getIsValidPiece() && piece->getColour() == Colour::BLACK) {
				this->blackKing = piece;
				break;
			}
		}
	}
	// find the white King
	for (auto piece: this->whitePieces) {
		if (piece != nullptr) {
			if (piece->getPieceType() == ChessPieceType::KING && piece->getIsValidPiece() && piece->getColour() == Colour::WHITE) {
				this->whiteKing = piece;
				break;
			}
		}
	}
	// for black king is not on the board
	if (this->blackKing == nullptr) {
		std::cout << "Error: Black King missing." << std::endl;
		return false;
	}
	// for white king is not on the board
	if (this->whiteKing == nullptr) {
		std::cout << "Error: White King missing." << std::endl;
		return false;
	}
	// checks that white king is not in check
	if (isCheck(Colour::WHITE)) {
		throw std::logic_error{"The White King may not start in check."};
	}
	// checks that black king is not in check
	if (isCheck(Colour::BLACK)) {
		throw std::logic_error{"The Black King may not start in check."};
	}
	return done;
}

/**
 * getWidth() returns the width of the board
 */
int Board::getWidth() const {
	return this->width;
}

/**
 * getHeight() returns the height of the board
 */
int Board::getHeight() const {
	return this->height;
}

/**
 * isValidCoordinate(row, col) checks that row and col are 
 * 	valid coordinates on the board.
 */
bool Board::isValidCoordinate(int row, int col) const {
	if ((row >= 0 && row < height) && (col >= 0 && col < width)) {
		return true;
	} else {
		std::invalid_argument("Invalid co-ordinate.");
	}
	return false;
}

/**
 * getSpot(row, col) returns the Spot on the board at the given row
 * 	and col
 * Note: if row or col are invalid then returns a nullptr
 */
std::shared_ptr<Spot> Board::getSpot(int row, int col) const {
	if (isValidCoordinate(row, col)) {
		return spots.at(row).at(col);
	}
	return nullptr;
}

/**
 * findWhiteKing() checks if there is exactly one white king 
 * 	on the board
 */
bool Board::findWhiteKing() {
	int whiteKing = 0;
	for (auto piece: this->whitePieces) {
		if (piece->getPieceType() == ChessPieceType::KING && piece->getIsValidPiece() && piece->getColour() == Colour::WHITE) {
			whiteKing = whiteKing + 1;
		}
	}
	if (whiteKing == 1) {
		return true;
	}
	return false;

}

/**
 * findBlackKing() checks if there is exactly one black king
 * 	on the board
 */
bool Board::findBlackKing() {
	// loops through the black pieces arary and finds the black king
	int blackKing = 0;
	for (auto piece: this->blackPieces) {
		if (piece->getPieceType() == ChessPieceType::KING && piece->getIsValidPiece() && piece->getColour() == Colour::BLACK) {
			blackKing = blackKing + 1;
		}
	}
	if (blackKing == 1) {
		return true;
	}
	return false;
}

/**
 * noPawns() checks if there are no pawns in the first
 * 	and last row of the board
 */
bool Board::noPawns() {
	bool firstRow = true;
	bool lastRow = true;
	// check for pawns in first row
	for (auto &p: spots.at(0)) {
		if (p->getPiece() != nullptr && p->getPiece()->getPieceType() == ChessPieceType::PAWN) {
			firstRow = false;
			break;
		}
	}
	// check for pawns in last row
	for (auto &p: spots.at(7)) {
		if (p->getPiece() != nullptr && p->getPiece()->getPieceType() == ChessPieceType::PAWN) {
			lastRow = false;
			break;
		}
	}

	return firstRow && lastRow;
}

/**
 * validPiece(piece) checks if piece is a valid 
 * 	chess piece that can be added to the board
 */
bool Board::validPiece(char piece) {
	// checks if the char piece is valid
	// if not -> throw error
	if (piece =='K' || piece == 'Q' || piece == 'B' || piece == 'N' || piece == 'R' || piece == 'P'||
			piece == 'p' || piece =='k' || piece == 'q' || piece == 'b' || piece == 'n' || piece == 'r') {
		return true;
	} else {
		throw std::invalid_argument("Invalid piece provided.");
	}
	return false;
}

/**
 * charToPieceType(piece) converts the piece
 * 	to the given valid chess piece type
 */
ChessPieceType Board::charToPieceType(char piece) {
	if (piece == 'K' || piece == 'k') {
		return ChessPieceType::KING;
	} else if (piece == 'Q' || piece == 'q') {
		return ChessPieceType::QUEEN;
	} else if (piece == 'B' || piece == 'b') {
		return ChessPieceType::BISHOP;
	} else if (piece == 'N' || piece == 'n') {
		return ChessPieceType::KNIGHT;
	} else if (piece == 'R' || piece == 'r') {
		return ChessPieceType::ROOK;
	} else if (piece == 'P' || piece == 'p') {
		return ChessPieceType::PAWN;
	} 
	return ChessPieceType::PAWN;
}

/**
 * findColour(piece) produces the color of the piece depending on the 
 * 	chess piece supplied
 * Note: Color is White for K,Q,N,R,B,P
 * 	 Colour is black for k,q,n,r,b,p
 * 	 Default colour is white if invalid piece supplied
 */
Colour Board::findColour(char piece) {
	if(!validPiece(piece)) {
		std::invalid_argument("Colour for chess piece cannot be determined.");
	}
	if (piece == 'K' || piece == 'Q' || piece == 'R' || piece == 'N' || piece == 'P' || piece == 'B') {
		return Colour::WHITE;
	} 
	if (piece == 'k' || piece == 'q' || piece == 'r' || piece == 'n' || piece == 'p' || piece == 'b') {
		return Colour::BLACK;
	}
	return Colour::WHITE;
}

/**
 * createChessPiece(pieceType, colour, row, col) creates a chess piece of the given pieceType,
 * 	with the given particular colour, and position as (row, col)
 * Note: if in valid chess piece type, colour, row or col then returns a nullptr
 */
std::shared_ptr<ChessPiece> Board::createChessPiece(ChessPieceType pieceType, Colour colour, int row, int col) {
	// checks if the colour is valid
	if (colour == Colour::WHITE || colour == Colour::BLACK) {
		// creates a piece of the specific valid type
		if (pieceType == ChessPieceType::PAWN) {
			auto piece = std::make_shared<Pawn>(pieceType, colour, row, col);
			return piece;
		} else if (pieceType == ChessPieceType::KING) {
			auto piece = std::make_shared<King>(pieceType, colour, row, col);
			return piece;
		} else if (pieceType == ChessPieceType::QUEEN) {
			auto piece = std::make_shared<Queen>(pieceType, colour, row, col);
			return piece;
		} else if (pieceType == ChessPieceType::BISHOP) {
			auto piece = std::make_shared<Bishop>(pieceType, colour, row, col);
			return piece;
		} else if (pieceType == ChessPieceType::KNIGHT) {
			auto piece = std::make_shared<Knight>(pieceType, colour, row, col);
			return piece;
		} else if (pieceType == ChessPieceType::ROOK) {
			auto piece  = std::make_shared<Rook>(pieceType, colour, row, col);
			return piece;
		}
	} 
	return nullptr;
}

/**
 * createEmptyBoard() creates an empty board
 * 	with no chess pieces on it
 */
void Board::createEmptyBoard() {
	// 2 for loops on width and height
	Colour spotColour;
	for (int r = 0; r < height; r++) {
		if (r % 2 == 1) {
			spotColour = Colour::WHITE;
		} else {
			spotColour = Colour::BLACK;
		}
		std::vector<std::shared_ptr<Spot>> rows;
		for (int c = 0; c < width; c++) {
			rows.push_back(std::make_shared<Spot>(r, c, spotColour, nullptr));
			if (spotColour == Colour::WHITE) {
				spotColour = Colour::BLACK;
			} else if (spotColour == Colour::BLACK) {
				spotColour = Colour::WHITE;
			} else {
				spotColour = Colour::WHITE;
			}
		}
		spots.push_back(rows);
	}
}

/**
 * addDefaultPieces() adds default pieces
 * 	to an empty board of defualt size 8 x 8
 * Note: will correctly add pieces to the board
 * 	given that it is default size of 8 x 8
 */
void Board::addDefaultPieces() {
	// initiliaze white pieces
	// white rook
	auto whiteRook = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Rook>(ChessPieceType::ROOK, Colour::WHITE, 0, 0));
	spots.at(0).at(0).get()->setPiece(whiteRook);
	whitePieces.push_back(whiteRook);

	// white Knight
	auto whiteKnight = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Knight>(ChessPieceType::KNIGHT, Colour::WHITE, 0, 1));
	spots.at(0).at(1).get()->setPiece(whiteKnight);
	whitePieces.push_back(whiteKnight);

	// white bishop
	auto whiteBishop = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Bishop>(ChessPieceType::BISHOP, Colour::WHITE, 0, 2));
	spots.at(0).at(2).get()->setPiece(whiteBishop);
	whitePieces.push_back(whiteBishop);

	// white Queen
	auto whiteQueen = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Queen>(ChessPieceType::QUEEN, Colour::WHITE, 0, 3));
	spots.at(0).at(3).get()->setPiece(whiteQueen);
	whitePieces.push_back(whiteQueen);

	// white King
	auto whiteKing = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<King>(ChessPieceType::KING, Colour::WHITE, 0, 4));
	spots.at(0).at(4).get()->setPiece(whiteKing);
	whitePieces.push_back(whiteKing);
	this->whiteKing = whiteKing; // tracking location of white king

	// white bishop2
	auto whiteBishop2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Bishop>(ChessPieceType::BISHOP, Colour::WHITE, 0, 5));
	spots.at(0).at(5).get()->setPiece(whiteBishop2);
	whitePieces.push_back(whiteBishop2);

	// white Knight2
	auto whiteKnight2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Knight>(ChessPieceType::KNIGHT, Colour::WHITE, 0, 6));
	spots.at(0).at(6).get()->setPiece(whiteKnight2);
	whitePieces.push_back(whiteKnight2);

	// white Rook2
	auto whiteRook2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Rook>(ChessPieceType::ROOK, Colour::WHITE, 0, 7));
	spots.at(0).at(7).get()->setPiece(whiteRook2);
	whitePieces.push_back(whiteRook2);

	// for white pawns - in row 1
	for(std::size_t white = 0; white < spots.at(1).size(); white++) {
		auto whitePawn = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Pawn>(ChessPieceType::PAWN, Colour::WHITE, 1, white));
		whitePieces.push_back(whitePawn);
		spots.at(1).at(white).get()->setPiece(whitePawn);
	}

	// initializing black pieces
	// black rook
	auto blackRook = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Rook>(ChessPieceType::ROOK, Colour::BLACK, 7, 0));
	spots.at(7).at(0).get()->setPiece(blackRook);
	blackPieces.push_back(blackRook);

	// black knight
	auto blackKnight = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Knight>(ChessPieceType::KNIGHT, Colour::BLACK, 7, 1));
	spots.at(7).at(1).get()->setPiece(blackKnight);
	blackPieces.push_back(blackKnight);

	// black bishop
	auto blackBishop = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Bishop>(ChessPieceType::BISHOP, Colour::BLACK, 7, 2));
	spots.at(7).at(2).get()->setPiece(blackBishop);
	blackPieces.push_back(blackBishop);

	// black Queen
	auto blackQueen = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Queen>(ChessPieceType::QUEEN, Colour::BLACK, 7, 3));
	spots.at(7).at(3).get()->setPiece(blackQueen);
	blackPieces.push_back(blackQueen);

	// black King
	auto blackKing = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<King>(ChessPieceType::KING, Colour::BLACK, 7, 4));
	spots.at(7).at(4).get()->setPiece(blackKing);
	blackPieces.push_back(blackKing);
	this->blackKing = blackKing; // tracking the location of the king

	// black bishop 2
	auto blackBishop2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Bishop>(ChessPieceType::BISHOP, Colour::BLACK, 7, 5));
	spots.at(7).at(5).get()->setPiece(blackBishop2);
	blackPieces.push_back(blackBishop2);

	// black knight2
	auto blackKnight2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Knight>(ChessPieceType::KNIGHT, Colour::BLACK, 7, 6));
	spots.at(7).at(6).get()->setPiece(blackKnight2);
	blackPieces.push_back(blackKnight2);

	// black rook2
	auto blackRook2 = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Rook>(ChessPieceType::ROOK, Colour::BLACK, 7, 7));
	spots.at(7).at(7).get()->setPiece(blackRook2);
	blackPieces.push_back(blackRook2);

	// black pawns
	for (std::size_t black = 0; black < spots.at(6).size(); black++) {
		auto blackPawn = std::dynamic_pointer_cast<ChessPiece>(std::make_shared<Pawn>(ChessPieceType::PAWN, Colour::BLACK, 6, black));
		spots.at(6).at(black).get()->setPiece(blackPawn);
		blackPieces.push_back(blackPawn);
	}

}

/**
 * operator<<(out, board) overloads the default output operator to 
 * 	print the bord object
 * Note: 1. White unoccupied spaces are denoted by a space
 *	 2. Black unoccupied spaces are denoted by an underscore
 *	 3. White chess pieces are denoted by uppercase letters
 *	 4. Black chess pieces are denoted by lowercase letters
 */
std::ostream &operator<<(std::ostream &out, const Board &board) {
	for (int rows = board.getHeight() - 1; rows >= 0; --rows) {
		out << (rows + 1) << " ";
		for (int cols = 0; cols < board.getWidth(); ++cols) {
			out << *(board.getSpot(rows, cols));
		}
		out << std::endl;
	}
	out << std::endl;
	out << "  abcdefgh " << std::endl;
	return out;
}

/**
 * getWhitePiece() returns the vector of white chess pieces
 * 	currently on the board
 * Note: This vector contains dead and alive pieces while
 * 	 the game is being played. When the game starts it
 * 	 contains all living pieces.
 */
std::vector<std::shared_ptr<ChessPiece>> &Board::getWhitePiece() {
    return whitePieces;
}

/**
 * getBlackPieces() returns the vector of black chess pieces
 * 	currently on the board
 * Note: This vector contains dead (captured) and alive (valid)
 * 	pieces while the game is being played. When the game
 * 	starts it contains all living (valid) pieces
 */ 
std::vector<std::shared_ptr<ChessPiece>> &Board::getBlackPiece() {
    return blackPieces;
}

/**
 * getWhiteKing() returns the white king on the board
 * Note: if no white king on the board, then returns
 * 	nullptr
 */
std::shared_ptr<ChessPiece> &Board::getWhiteKing() {
	return whiteKing;
}

/**
 * getBlackKing() returns the black king on the board
 * Note: if no black king on the board, then returns 
 * 	 nullptr
 */
std::shared_ptr<ChessPiece> &Board::getBlackKing() {
	return blackKing;
}

/**
 * isCheck(start, end, colour) checks if the king of the 
 * 	given colour is in check
 * Note: start denotes the starting position of the piece being moved
 * 	 end denotes the ending position of the peice being moved
 */
bool Board::isCheck(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Colour colour) {
	// storing the pieces 
	std::shared_ptr<ChessPiece> startPiece = start->getPiece();
	std::shared_ptr<ChessPiece> endPiece = end->getPiece();
	int oldRow = start->getRow();
	int oldCol = start->getCol();
	// moving the pieces from start to end spot
	start->setPiece(nullptr);
	end->setPiece(startPiece);
	if (startPiece->getPieceType() == ChessPieceType::KING) {
		startPiece->setRow(end->getRow());
		startPiece->setCol(end->getCol());
	}
	bool check = isCheck(colour);

	// undo the move
	start->setPiece(startPiece);
	end->setPiece(endPiece);
	startPiece->setRow(oldRow);
	startPiece->setCol(oldCol);
	return check;
}

/**
 * isCheck(colour) checks if the king of the given 
 * 	colour is in check
 * Note: if colour is white then checks if white king
 * 	 is in check
 * 	 if colout is black then checks if the black king
 * 	 is in check
 */
bool Board::isCheck(Colour colour) {
	std::shared_ptr<ChessPiece> checkKing = nullptr;
	std::vector<std::shared_ptr<ChessPiece>> attackPieces;
	if (colour == Colour::WHITE) {
		checkKing = this->whiteKing;
		attackPieces = this->blackPieces;
	} else if (colour == Colour::BLACK) {
		checkKing = this->blackKing;
		attackPieces = this->whitePieces;
	}

	if (checkKing == nullptr) {
		if (colour == Colour::WHITE) {
			std::invalid_argument("Cannot determine check. White King not found.");
		} else {
			std::invalid_argument("Cannot determine check. Black King not found.");
		}
		return false;
	}
	bool check = false;
	for (auto piece: attackPieces) {
		if (piece != nullptr && piece->getIsValidPiece() && piece->validMove(this->getSpot(piece->getRow(), piece->getCol()), 
					this->getSpot(checkKing->getRow(), checkKing->getCol()), this, false)) {
			check = true;
			break;
		}
	}
	return check;
}

/**
 * isCheckmate(colour) checks if the king of the given colour is in checkmate
 * Note: if colour is white, then checks if the white king is in checkmate
 * 	 if colour is black, then checks if the black king is in checkmate
 */
bool Board::isCheckmate(Colour colour) {
	if (!isCheck(colour)) return false;
	std::shared_ptr<ChessPiece> king = nullptr;
	if (colour == Colour::WHITE) {
		king = this->whiteKing;
	} else if (colour == Colour::BLACK) {
		king = this->blackKing;
	}
	int row = 0;
	int col = 0;
	bool checkmate = false;
	bool legal = false;
	if (king == nullptr) {
		if (colour == Colour::WHITE) {
			std::invalid_argument("Cannot determine checkmate. White King not found.");
		} else {
			std::invalid_argument("Cannot determine checkmate. Black King not found.");
		}
		return false;
	} else {
		row = king->getRow();
		col = king->getCol();
	}

	std::vector<std::vector<int>> moves = {{row + 1, col - 1}, {row , col - 1}, {row - 1, col - 1}, {row - 1, col},
		{row - 1, col - 1}, {row, col + 1}, {row + 1, col + 1}, {row + 1, col}, {row, col}};
	
	for (std::size_t i = 0; i < moves.size(); i++) {
		if ((moves.at(i).at(0) >= 0 && moves.at(i).at(0) < this->width) && (moves.at(i).at(1) >= 0 && moves.at(i).at(1) < this->height)) {
			if (king->validMove(this->getSpot(king->getRow(), king->getCol()), this->getSpot(moves.at(i).at(0), moves.at(i).at(1)), this, true)) {
				legal = true;
				break;
			}
		}
	}
	if (legal) {
		checkmate = false;
	} else {
		checkmate = true;
	}	
	return checkmate;
}

/**
 * isStalemate(colour) checks if the player of the given colour is 
 * 	in stalemate (aka if it has anymore legal moves or not)
 */
bool Board::isStalemate(Colour colour) {
	std::vector<std::shared_ptr<ChessPiece>> stalematePieces;
	if (isCheck(colour)) return false;
	if (colour == Colour::WHITE) {
		stalematePieces = this->whitePieces;
	} 
	if (colour == Colour::BLACK) {
		stalematePieces = this->blackPieces;
	}
	bool stalemate = false;
	bool legal = false;
	for (auto piece: stalematePieces) {
		if (piece != nullptr) {
			std::vector<int> move = piece->randomValidMove(this->getSpot(piece->getRow(), piece->getCol()), this);
			if (move.size() != 0) {
				legal = true;
				break;
			}
		}
	}

	if (legal) {
		stalemate = false;
	} else {
		stalemate = true;
	}
	return stalemate;
}

