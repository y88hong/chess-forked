#include "move.h"
#include "chessPiece.h"
#include "spot.h"
#include "board.h"

Move::Move(std::shared_ptr<ChessPiece> piecemove, std::shared_ptr<Spot> start,
    std::shared_ptr<Spot> end, std::shared_ptr<ChessPiece> piecekilled, std::shared_ptr<ChessPiece> piecepromote) :
    pieceMoved{ piecemove }, start{ start }, end{ end }, pieceKilled{ piecekilled }, piecePromote{ piecepromote } {}


std::shared_ptr<ChessPiece> Move::getPieceMoved() {
    return pieceMoved;
}
bool Move::isPawnMoveTwo() {
    return (pieceMoved->getPieceType() == ChessPieceType::PAWN &&
        ((start->getRow() - end->getRow() == 2) || (start->getRow() - end->getRow() == -2)));
}
int Move::getDirection() {
    return start->getRow() - end->getRow();
}
// invariant: the end spot is empty
void Move::movePiece() {
    // the end spot piece identity is already checked, so just kill it
    if (end->isOccupied()) {
        pieceKilled = end->removePiece();
        pieceKilled->setIsValidPiece(false);
    }
    // spot: move from start spot to end spot
    // check occupied for castling
    if (start->isOccupied()) {
        start->removePiece();
    }
    end->setPiece(pieceMoved);
    // update row, col, hasMoved in chess
    pieceMoved->setRow(end->getRow());
    pieceMoved->setCol(end->getCol());
    pieceMoved->setHasMoved(true);
}

void Move::moveEnPassant(std::shared_ptr<Board> board) {
    // get captured Pawn
    pieceKilled = board->getSpot(start->getRow(), end->getCol())->removePiece();
    pieceKilled->setIsValidPiece(false);
    // move and update moving piece
    this->movePiece();
}

std::shared_ptr<Move> Move::moveCastle(std::shared_ptr<Board> board) {
    // move King
    this->movePiece();
    // Move Rook
    if (end->getCol() == 2) {
        std::shared_ptr<ChessPiece> rook = board->getSpot(start->getRow(), 0)->removePiece(); // note the rook is removed from start spot
        std::shared_ptr<Spot> rookStart = board->getSpot(start->getRow(), 0);
        std::shared_ptr<Spot> rookEnd = board->getSpot(start->getRow(), 3);

        std::shared_ptr<Move> rookMove =
            std::make_shared<Move>(rook, rookStart, rookEnd);
        rookMove->movePiece();
        return rookMove;
    }
    else {
        std::shared_ptr<ChessPiece> rook = board->getSpot(start->getRow(), 7)->removePiece();
        std::shared_ptr<Spot> rookStart = board->getSpot(start->getRow(), 7);
        std::shared_ptr<Spot> rookEnd = board->getSpot(start->getRow(), 5);

        std::shared_ptr<Move> rookMove =
            std::make_shared<Move>(rook, rookStart, rookEnd);
        rookMove->movePiece();
        return rookMove;
    }
}

// promote will kill the original pawn
// invariant: the promoted hasMoved field shoudl be true, promoted should be true
void Move::movePromote() {
    // kill the Pawn
    start->removePiece();
    pieceMoved->setIsValidPiece(false);
    // modify killed piece validity (the killed piece is already removed from board)
    if (pieceKilled) {
        pieceKilled->setIsValidPiece(false);
    }
    // promote is already moved to spot, row, col index already set
    // double check in case...
    if (!end->isOccupied()) {
        end->setPiece(piecePromote);
    }
    piecePromote->setHasMoved(true);
}

// undo one step
void Move::undo(std::shared_ptr<Board> board) {
    // change spot
    end->removePiece(); 
    start->setPiece(pieceMoved);
    // change fields in pieceMoved
    // if promotion: revive Pawn if it's promoted
    if (pieceMoved->getIsValidPiece() == false) {
        pieceMoved->setIsValidPiece(true);
    }
    // update row, col, hasMoved in chess
    pieceMoved->setRow(start->getRow());
    pieceMoved->setCol(start->getCol());

    // restored deadPiece, if it's not ptomotion
    if (pieceKilled) {
        pieceKilled->setIsValidPiece(true);
        auto tomb = board->getSpot(pieceKilled->getRow(), pieceKilled->getCol());
        tomb->setPiece(pieceKilled);
    }

}

std::shared_ptr<ChessPiece> Move::getPieceKilled() {
    return pieceKilled;
}

bool Move::isKingMoveTwoCol() {
    return (pieceMoved->getPieceType() == ChessPieceType::KING &&
        ((start->getCol() - end->getCol() == 2) || (start->getCol() - end->getCol() == -2)));
}
int Move::getKingDirection() {
    return start->getCol() - end->getCol();
}

Move::~Move() {}

