#ifndef _CHESSPIECE_H
#define _CHESSPIECE_H

#include <iostream>
#include <memory>
#include <vector>
#include <random>
#include <algorithm>
#include "chessPieceType.h"
#include "colour.h"

class Spot;
class Board;

class ChessPiece {
    const ChessPieceType name;
    const Colour colour;
    bool hasMoved, isValidPiece;
    int row, col;

    protected:
    bool isValidCapture(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end);

    public:
    ChessPiece(ChessPieceType name, Colour colour, int row, int col);
    ChessPiece(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col);
    ChessPieceType getPieceType() const;
    Colour getColour() const;
    bool getHasMoved() const;
    void setHasMoved(bool hasMoved);
    bool getIsValidPiece() const;
    void setIsValidPiece(bool isValidPiece);
    bool isColour(Colour colour) const;
    int getRow() const;
    int getCol() const;
    void setRow(int row);
    void setCol(int col);
    char pieceTypeAsChar() const;

    // note: shared_ptr methods are wrappers that call the true implementation which utilze Board *
    virtual bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck = true) = 0; // true implementation
    virtual bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck = true) = 0; // wrapper

    // randomValidMove(...) returns a vector indicating the row and column number of the start, then end moves
    // * ex. <1,2,3,4> indicates start at row 1, col 2 and move to row 3, col 4
    virtual std::vector<int> randomValidMove(std::shared_ptr<Spot> start, Board *board) = 0;
    virtual std::vector<int> randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) = 0;

    friend std::ostream& operator<<(std::ostream &os, const ChessPiece &piece);
};

#endif

