#include "textobserver.h"
#include "displayboard.h"
#include "board.h"
#include <iostream>

TextObserver::TextObserver(std::shared_ptr<DisplayBoard> sub) : subject{sub}
{
    subject->attach(this);
}
TextObserver::~TextObserver()
{
    subject->detach(this);
}
void TextObserver::notify()
{
    std::cout << *(subject->getPrintBoard()) << std::endl;
}

