#ifndef _INTERMEDIATE_H
#define _INTERMEDIATE_H

#include <vector>
#include <memory>
#include "strategy.h"
#include "colour.h"

class Intermediate : public Strategy {
public:
    std::vector<int> calculateMove(std::shared_ptr<Board> board, Colour colour,std::vector<std::shared_ptr<Move>> allMoves);
};

#endif

