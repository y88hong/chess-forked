#include "bishop.h"
#include "board.h"
#include "spot.h"

Bishop::Bishop(ChessPieceType name, Colour colour, int row, int col):
    ChessPiece(name, colour, row, col)
{ }

Bishop::Bishop(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col):
    ChessPiece(name, colour, hasMoved, isValidPiece, row, col)
{ }

bool Bishop::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck) {
    return validMove(start, end, &*board, checkCheck);
}

bool Bishop::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck) {
    if (start == nullptr || end == nullptr || start == end) return false;

    int startRow = start->getRow();
    int startCol = start->getCol();
    int endRow = end->getRow();
    int endCol = end->getCol();

    // Diagonal move
    int hmoveDiff = endCol - startCol;
    int vmoveDiff = endRow - startRow;
    int verticalDir = 1; // moving "upwards" - vertical direction of diagonal movement
    int horizontalDir = 1; // moving "rightwards" - horizontal direction of diagonal movement
    if (hmoveDiff < 0) {
        hmoveDiff *= -1;
        horizontalDir = -1;
    }
    if (vmoveDiff < 0) {
        vmoveDiff *= -1;
        verticalDir = -1;
    }
    if (hmoveDiff == vmoveDiff) {
        int i = startCol + horizontalDir;
        int j = startRow + verticalDir;
        while (board->isValidCoordinate(j, i) && i != endCol && j != endRow) {
            std::shared_ptr<Spot> spotJI = board->getSpot(j, i);
            if (spotJI && spotJI->isOccupied()) return false;
            i += horizontalDir;
            j += verticalDir;
        }
        return ChessPiece::isValidCapture(start, end) && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour()));
    }
    return false;
}

std::vector<int> Bishop::randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) {
    return randomValidMove(start, &*board);
}

std::vector<int> Bishop::randomValidMove(std::shared_ptr<Spot> start, Board *board) {
    // random number generator courtesy of https://en.cppreference.com/w/cpp/numeric/random
    // generate random direction and length and see if that move is valid
    std::vector<int> lengths{};
    std::vector<int> directions{};
    std::vector<int> retval{};

    for (int i = 1; i <= 4; ++i) {
        directions.push_back(i);
    }
    for (int i = 1; i <= 7; ++i) {
        lengths.push_back(i);
    }

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(directions.begin(), directions.end(), g);
    std::shuffle(lengths.begin(), lengths.end(), g);

    int startCol = start->getCol();
    int startRow = start->getRow();

    int hdir = 0, vdir = 0;
    int randDirection = directions.back();
    if (randDirection == 1) {
        hdir = -1;
        vdir = -1;
    } else if (randDirection == 2) {
        hdir = 1;
        vdir = -1;
    } else if (randDirection == 3) {
        hdir = -1;
        vdir = 1;
    } else {
        hdir = 1;
        vdir = 1;
    }
    directions.pop_back();
    int length = lengths.back();
    lengths.pop_back();
    while (!(directions.empty() && lengths.empty())) {

        if (directions.empty()) break;
        if (lengths.empty()) {
            for (int i = 1; i <= 7; ++i) { // reset possible lengths, get new movement direction
                lengths.push_back(i);
            }
            std::shuffle(lengths.begin(), lengths.end(), g);
            int randDirection = directions.back();
            if (randDirection == 1) {
                hdir = -1;
                vdir = -1;
            } else if (randDirection == 2) {
                hdir = 1;
                vdir = -1;
            } else if (randDirection == 3) {
                hdir = -1;
                vdir = 1;
            } else {
                hdir = 1;
                vdir = 1;
            }
            directions.pop_back();
        }
    
        int endCol = startCol + (hdir * length);
        int endRow = startRow + (vdir * length);
        if (board->isValidCoordinate(endRow, endCol) && validMove(start, board->getSpot(endRow, endCol), board)) {
            retval.emplace_back(startRow);
            retval.emplace_back(startCol);
            retval.emplace_back(endRow);    
            retval.emplace_back(endCol);   
            break;
        }
        length = lengths.back();
        lengths.pop_back();
    }
    return retval;
    
}
