#ifndef _EXPERT_H_
#define _EXPERT_H_

#include <vector>
#include <memory>
#include "strategy.h"
#include "colour.h"


class Expert : public Strategy {
    bool inDanger(std::shared_ptr<Board> board, std::shared_ptr<ChessPiece> piece, std::shared_ptr<Spot> newEnd, Colour colour);
    std::vector<int> captureMoveTrans(std::vector<moveSpot> moves);
    std::vector<int> kingSpecial(std::shared_ptr<Board> board, std::shared_ptr<Spot> kingSpot, Colour colour);
public:
    std::vector<int> calculateMove(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves);
};


#endif