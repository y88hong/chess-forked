#include "window.h"
#include "graphicsobserver.h"
#include <iostream>
#include <memory>
#include <string>
#include "colour.h"
#include "chessPieceType.h"
#include "spot.h"
#include "board.h"
#include "chessPiece.h"

GraphicsObserver::GraphicsObserver(std::shared_ptr<DisplayBoard> subject, int boardHeight, int boardWidth):
	subject{subject}, boardHeight{boardHeight}, boardWidth{boardWidth} {
		// initializing the observer with the subject
		display = std::make_shared<Xwindow>((this->boardHeight * 20), (this->boardWidth * 20));
		// registering the observer with the subject
		subject->attach(this);
	}

// notify for printing
void GraphicsObserver::notify() {
	// retriving the board from the subject
	std::shared_ptr<Board> rendered = subject->getPrintBoard();
	// loop thorough and display characters
	for(int rows = this->boardHeight - 1; rows >= 0; --rows) {
		// print the rows - 8, 7, ...
		for (int cols = 0; cols < this->boardWidth; ++cols) {
			// get the spot
			// if occupied -> then get the colour of that sqaure and print the string
			// if not occupied -> get the colour and print an empty string
			std::shared_ptr<Spot> spot = rendered->getSpot(rows, cols);
			// for character -> character based on the piece (white - uppercase, black - lowercase)
			if (spot->getSpotColour() == Colour::WHITE) {
				display->fillRectangle((cols * 20), (rows * 20), 20, 20, display->White);
			}
			if (spot->getSpotColour() == Colour::BLACK) {
				display->fillRectangle((cols * 20), (rows * 20), 20, 20, display->Brown);
			}
		}
	}
	for(int rows = 0; rows < this->boardHeight; ++rows) {
		// print the rows - 8, 7, ...
		int height = this->boardHeight;
		for (int cols = 0; cols < this->boardWidth; ++cols) {
			// get the spot
			// if occupied -> then get the colour of that sqaure and print the string
			// if not occupied -> get the colour and print an empty string
			std::shared_ptr<Spot> spot = rendered->getSpot(rows, cols);
			// for character -> character based on the piece (white - uppercase, black - lowercase)
			if (spot->isOccupied()) {
				std::shared_ptr<ChessPiece> piece = spot->getPiece();
				if (piece->getColour() == Colour::WHITE) {
					if (piece->getPieceType() == ChessPieceType::PAWN) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20 + 12, "P");
					} else if (piece->getPieceType() == ChessPieceType::KING) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20 + 12, "K");
					} else if (piece->getPieceType() == ChessPieceType::ROOK) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "R");
					} else if (piece->getPieceType() == ChessPieceType::BISHOP) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "B");
					} else if (piece->getPieceType() == ChessPieceType::QUEEN) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "Q");
					} else if (piece->getPieceType() == ChessPieceType::KNIGHT) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "N");
					}
				}
				if (spot->getPiece()->getColour() == Colour::BLACK) {
					if (piece->getPieceType() == ChessPieceType::PAWN) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "p");
					} else if (piece->getPieceType() == ChessPieceType::KING) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "k");
					} else if (piece->getPieceType() == ChessPieceType::ROOK) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "r");
					} else if (piece->getPieceType() == ChessPieceType::BISHOP) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "b");
					} else if (piece->getPieceType() == ChessPieceType::QUEEN) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "q");
					} else if (piece->getPieceType() == ChessPieceType::KNIGHT) {
						display->drawString(cols * 20 + 8, (height - rows - 1) * 20+ 12, "n");
					}
				}
			}
		}
	}
}

// destructor
GraphicsObserver::~GraphicsObserver() {
	subject->detach(this);
}
