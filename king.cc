#include "spot.h"
#include "board.h"
#include "king.h"
#include "chessPieceType.h"
#include "colour.h"

King::King(ChessPieceType name, Colour colour, int row, int col):
    ChessPiece(name, colour, row, col)
{ }

King::King(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col):
    ChessPiece(name, colour, hasMoved, isValidPiece, row, col)
{ }
 
bool King::isValidCastle(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, int hmove) {
    return isValidCastle(start, end, &*board, hmove);
}

// hmove indicates the direction of the castle (left is negative)
// * invariant: hmove is always 1 (castle rightwards) or -1 (castle leftwards)
bool King::isValidCastle(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, int hmove) {

    int startCol = start->getCol();

    // check if king is currently in check
    if (board->isCheck(ChessPiece::getColour())) return false;

    // king at bottom of board
    const int rrookCol = board->getWidth() - 1;
    int rookRow = 0; // default checking white castle
    int rookCol = 0; // default, left rook
    if (ChessPiece::getColour() == Colour::BLACK) {
        rookRow = board->getHeight() - 1;
    }
    
    if (hmove > 0) {
        rookCol = rrookCol;
    }
    // checking if there is a rook in the expected spot
    if (!board->getSpot(rookRow, rookCol)->isOccupied()) {
        return false;
    }
    // checking if rook has moved
    if (board->getSpot(rookRow, rookCol)->getPiece()->getHasMoved()) { // king castles rightwards
        return false;
    }
    int hdir = 1; // positive indicates king moves rightward
    if (hmove < 0) { hdir = -1; }

    // check if there are any chess pieces between king and rook
    for (int i = startCol + hdir; i != rookCol; i += hdir) {
        if (board->getSpot(rookRow, i)->isOccupied()) return false;
    }

    return true;
}

bool King::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck) {
    return validMove(start, end, &*board, checkCheck);
}

bool King::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck) {
    if (start == nullptr || end == nullptr || start == end) return false;

    int startRow = start->getRow();
    int startCol = start->getCol();
    int endRow = end->getRow();
    int endCol = end->getCol();

    // check for castling
    int hmove = endCol - startCol; // how many squares is horizontally moved
    if (!ChessPiece::getHasMoved() && startRow == endRow && (hmove == 2 || hmove == -2) && (startRow == 0 || startRow == board->getHeight() - 1)) {
      bool validCastle = isValidCastle(start, end, board, hmove);
      return validCastle && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour()));
    }
    // check for normal, 1 piece moves
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            if (i == 0 && j == 0) continue;
            if (startCol + i == endCol && startRow + j == endRow) {
		        return ChessPiece::isValidCapture(start, end) && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour()));
	        }
        }
    }
    return false;
}

std::vector<int> King::randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) {
    return randomValidMove(start, &*board);
}

// random move DOES NOT include the potential to castle.
std::vector<int> King::randomValidMove(std::shared_ptr<Spot> start, Board *board) {
    // random number generator courtesy of https://en.cppreference.com/w/cpp/numeric/random
    // generate random move by brute forcing through all 8 possibilities
    std::vector<int> retval{};
    std::vector<int> choices{}; // for control flow below
    for (int i = 1; i <= 8; ++i) {
        choices.push_back(i);
    }
    int startCol = start->getCol();
    int startRow = start->getRow();
    int hdir = 0;
    int vdir = 0;
    int endCol = -1;
    int endRow = -1;

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(choices.begin(), choices.end(), g);
    
    while (!choices.empty()) {
        int choice = choices.back();
        choices.pop_back();
        if (choice == 1) {
            hdir = -1;
            vdir = 1;
        } else if (choice == 2) {
            hdir = 0;
            vdir = 1;
        } else if (choice == 3) {
            hdir = 1;
            vdir = 1;
        } else if (choice == 4) {
            hdir = -1;
            vdir = 0;
        } else if (choice == 5) {
            hdir = 1;
            vdir = 0;
        } else if (choice == 6) {
            hdir = -1;
            vdir = -1;
        } else if (choice == 7) {
            hdir = 0;
            vdir = -1;
        } else {
            hdir = 1;
            vdir = -1;
        }
        endRow = startRow + vdir;
        endCol = startCol + hdir;
        if (board->isValidCoordinate(endRow, endCol) && validMove(start, board->getSpot(endRow, endCol), board)) {
            retval.emplace_back(startRow);
            retval.emplace_back(startCol);
            retval.emplace_back(endRow);    
            retval.emplace_back(endCol);   
            break;
        }
    }
    return retval;
}

