#ifndef _SUBJECT_H_
#define _SUBJECT_H_

#include "observer.h"
#include <vector>
#include <memory>

// abstract Subject

class Subject
{
    std::vector<Observer *> observers;

public:
    Subject();
    void attach(Observer *o);
    void detach(Observer *o); // use raw pointer cause the unique_ptr for o already exist
    void notifyObservers();
    virtual ~Subject() = 0;
};

#endif

