#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "board.h"
#include "player.h"
#include "gamestatus.h"
#include "human.h"
#include "computer.h"
#include "move.h"
#include "game.h"
#include "chessPiece.h"
#include "colour.h"
#include "chessPieceType.h"
#include "spot.h"
#include "pawn.h"

// constructor
Game::Game():board{std::make_shared<Board>(8, 8, true)}, currentPlayer{nullptr}, status{GameStatus::INACTIVE}, firstTurn{Colour::WHITE}
{}

// returns the board object
std::shared_ptr<Board> Game::getBoard() const {
	if (this->board!= nullptr) {
		return this->board;
	} else {
	   // throw error
	}
	return nullptr;
}

//returns a pointer to the current player
std::shared_ptr<Player> Game::getCurrentPlayer() const {
	if (this->currentPlayer != nullptr) {
		return this->currentPlayer;
	} else {
		// throw an error
	}
	return nullptr;
}

// returns the Player according to their colour
std::shared_ptr<Player> Game::getPlayer(std::string colour) const {
	// white player always at index 0
	// black player always ay indec 1
	Colour playerColour = convertToColour(colour);
	if (playerColour == Colour::WHITE) {
		return this->players.at(0);
	} else if (playerColour == Colour::BLACK) {
		return this->players.at(1);
	} else {
		// no such player exists error
		return nullptr;
	}
}

// private method
Colour Game::convertToColour(std::string colour) const {
	if (colour == "white") {
		return Colour::WHITE;
	} else if (colour == "black") {
		return Colour::BLACK;
	} else {
		// or throw an error
		return Colour::WHITE;
	}
}
		
// returns the previous move 
std::shared_ptr<Move> Game::getPreviousMove() const {
	if (this->moves.size() > 0) {
		return this->moves.back();
	} else {
		// error
	}
	return nullptr;
}

// returns the status of the game
GameStatus Game::getStatus() const {
	return this->status;
}


void Game::resign() {
	if (this->currentPlayer->getSide() == Colour::WHITE) {
		std::cout << "White resigns." << std::endl;
		this->status = GameStatus::BLACK_WIN;
	} else if (this->currentPlayer->getSide() == Colour::BLACK) {
		std::cout << "Black resigns." << std::endl;
		this->status = GameStatus::WHITE_WIN;
	} else {
		this->status = GameStatus::RESIGNATION;
	}
}

// undo player move, player version, change side
void Game::undoMove() {
	if (moves.size() == 0) {
		throw std::invalid_argument{ "there is no move" };
		return;
	}
	auto lastPieceMove = moves.at(moves.size() - 1)->getPieceMoved();
    moves.at(moves.size() - 1)->undo(board);
    moves.pop_back();
    bool isStillMoved = stillMoved(lastPieceMove, moves);
	lastPieceMove->setHasMoved(isStillMoved);
	// if last piece moved and next piece moved has the same colour
	if (moves.size() > 0 &&
		lastPieceMove->getColour() == moves.at(moves.size() - 1)->getPieceMoved()->getColour()) {
		// this is castling, change player's field
		currentPlayer->setHasCastle(false);
		undoMove();
		return;
	}

	// add logic to switch turns here
	currentPlayer = findOpponent();
}

// private version, no change in side, nor change in player's field has caslted
void Game::undoPlayerMove() {
	if (moves.size() == 0) {
		throw std::invalid_argument{ "there is no move (private undo)" };
		return;
	}
	auto lastPieceMove = moves.at(moves.size() - 1)->getPieceMoved();
    moves.at(moves.size() - 1)->undo(board);
    moves.pop_back();
    bool isStillMoved = stillMoved(lastPieceMove, moves);
    lastPieceMove->setHasMoved(isStillMoved);
    if (lastPieceMove->getColour() == moves.at(moves.size() - 1)->getPieceMoved()->getColour()) {
		this->undoPlayerMove();
		return;
	}
}



// addPlayer
void Game::addPlayer(bool isHuman, Colour playerColour, int level) {
	// if isHuman is true -> add human player
	// if isHuman is false -> add computer player
	// 1. check the level is between 1 and 4 (inclusive)
	// push player into players vector
	bool colour = false;
	// checking if player colour is valid
	if (playerColour == Colour::WHITE || playerColour == Colour::BLACK) {
		colour = true;
	} else {
		// throw an error
	}

	// !!!
	// pass to Player constructor the king's spot (player alr. tracks it anyways)
	if (isHuman && colour && level == 0) {
		if (playerColour == Colour::WHITE) {
			auto human = std::dynamic_pointer_cast<Player>(std::make_shared<Human>(board->getWhitePiece(), false, isHuman, playerColour));
			players.push_back(human);

		} 
		if (playerColour == Colour::BLACK) {
			auto human = std::dynamic_pointer_cast<Player>(std::make_shared<Human>(board->getBlackPiece(), false, isHuman, playerColour));
			players.push_back(human);
		}
	}
	// if player is computer
	// check if level is okay
	bool correctLevel = false;
	if (level >= 1 && level <= 4) {
		correctLevel = true;
	} else {
		// throw an error
	}
	//create a computer player
	if (!isHuman && colour && correctLevel) {
		if (playerColour == Colour::WHITE) {
			auto computer = std::dynamic_pointer_cast<Player>(std::make_shared<Computer>
					(level, board->getWhitePiece(), false, isHuman, playerColour));
			players.push_back(computer);

		} 
		if (playerColour == Colour::BLACK) {
			auto computer = std::dynamic_pointer_cast<Player>(std::make_shared<Computer>
					(level, board->getBlackPiece(), false, isHuman, playerColour));
			players.push_back(computer);

		}
	}
}

// add piece to board
void Game::addPiece(int row, int col, char piece) {
	if (this->board != nullptr) {
		board->addPiece(row, col, piece);
	} else {
		// throw an error
	}
}

// remove piece 
void Game::removePiece(int row, int col) {
	if (this->board != nullptr) {
		board->removePiece(row, col);
	} else {
		// throw an error
	}
}

// change turn
void Game::changeTurn(std::string turn) {
	// if white -> set the current player to white player
	// if black -> set the current player to black player
	for (auto &p: turn) {
		p = std::tolower(p);
	}
	if (turn == "white" || turn == "black") {
		if (turn == "white") {
			this->firstTurn = Colour::WHITE;
		}
		if (turn == "black") {
			this->firstTurn = Colour::BLACK;
		}
	} else {
		throw std::logic_error{"Invalid player colour provided."};
	}
}

// make move for players with the given start and end row/col, and promoteChar representing promoted piece
// check validity of the move through making attempted move (which will modify the board), using Move methods
	// If the move is invalid, throw exception, undo attempted move.
	// If the move is valid, attempted move is kept, player is switched to the other side
void Game::playerMove(int startRow, int startCol, int endRow, int endCol, char promoteChar) {
	// if player is computer, get startRow, startCol, endRow, endCol, promote Char through computer's move method
	if (!currentPlayer->getIsHuman()) {
		std::vector<int> index = static_cast<Computer *>(&*currentPlayer)->computerMove(board, currentPlayer->getSide(), moves);
		startRow = index.at(0);
		startCol = index.at(1);
		endRow = index.at(2);
		endCol = index.at(3);
		if (index.size() == 5) {
			promoteChar = index.at(4);
		}
	}

	// the following check is for human move only
	if (currentPlayer->getIsHuman()) {
		// check coordinate
		if ((!board->isValidCoordinate(startRow, startCol)) || (!board->isValidCoordinate(endRow, endCol))) {
			throw std::invalid_argument{ "invalid start/end coordinates" };
			return;
		}
		std::shared_ptr<Spot> startSpot = board->getSpot(startRow, startCol);
		std::shared_ptr<Spot> endSpot = board->getSpot(endRow, endCol);
		// check if there is a piece on startSpot
		if (!startSpot->isOccupied())     {
			throw std::invalid_argument{ "start spot is empty" };
			return;
		}
		std::shared_ptr<ChessPiece> chessMoved = startSpot->getPiece();
		// check if the chess belongs to player
		if (currentPlayer->getSide() != chessMoved->getColour())     {
			throw std::invalid_argument{ "this is not your chess" };
			return;
		}
		// check valid promotion
		// promoteChar check
		if (promoteChar != '\0' && !isValidCharOfPromotionPiece(promoteChar))     {
			throw std::invalid_argument{ "invalid promotion type" };
			return;
		}
		// promote position check
		if (!(endRow == 0 || endRow == 7) && promoteChar != '\0' && isValidCharOfPromotionPiece(promoteChar)) {
			throw std::invalid_argument{ "invalid line for promotion" };
			return;
		}
		// promotion position check (for not promoting after reach the bottom) 
		if ((endRow == 0 || endRow == 7) && promoteChar == '\0' && chessMoved->getPieceType() == ChessPieceType::PAWN) {
			throw std::invalid_argument{ "Pawn must be promoted when reach the other side" };
			return;
		}
		// promote color check
		if ((islower(promoteChar) && currentPlayer->getSide() == Colour::WHITE) ||
			(isupper(promoteChar) && currentPlayer->getSide() == Colour::BLACK)) {
			throw std::invalid_argument("invalid promotion color. For white chesspieces, please use upper cases. For black chesspieces, use lower cases");
			return;
		}
	}
	std::shared_ptr<Spot> startSpot = board->getSpot(startRow, startCol);
	std::shared_ptr<Spot> endSpot = board->getSpot(endRow, endCol);
	std::shared_ptr<ChessPiece> chessMoved = startSpot->getPiece(); 

	// check whether the move is valid (follow the chess rule)
	// check for Pawn
	if (ChessPieceType::PAWN == chessMoved->getPieceType())
    {
        // if promotion
        if (promoteChar != '\0') {
            // check validity
            if (!chessMoved->validMove(startSpot, endSpot, board)) {
                throw std::invalid_argument{"this is not a legal move (promotion)"};
                return;
			}
			// remove the piece at the end spot
			auto pieceKilled = board->getSpot(endRow, endCol)->removePiece();
			// create promoted piece
            addPiece(endSpot->getRow(), endSpot->getCol(), promoteChar); 
            std::shared_ptr<ChessPiece> promotePiece = endSpot->getPiece();
            // make attempted move object
            auto attempMove = std::make_shared<Move>(chessMoved, startSpot, endSpot, pieceKilled, promotePiece);
            attempMove->movePromote();
            moves.push_back(attempMove);
            // check if this move put its own king in check
			if (currentPlayer->getIsHuman() && myKingDanger()) {
				// king will be in danger, undo the move
				undoPlayerMove();
                throw std::invalid_argument{"Your king will be doomed (promotion)"};
                return;
			} else {
				// move success switch side
				auto nextPlayer = findOpponent();
				currentPlayer = nextPlayer;
			}
			// promotion check & move end
		} else if (startCol != endCol && board->getSpot(startRow, endCol)->isOccupied() &&
			moves.back()->isPawnMoveTwo()) {
            // check for en Passant
            // check validity
            if (currentPlayer->getIsHuman() && !static_cast<Pawn *>(&*chessMoved)->validMove(true, startSpot, endSpot, board)){
                throw std::invalid_argument{"Invalid Move (En Passant)"};
                return;
            }
            // make attempted move
            auto attempMove = std::make_shared<Move>(chessMoved, startSpot, endSpot);
            moves.push_back(attempMove);
            attempMove->moveEnPassant(board);
            // check if this move put its own king in check
			if (currentPlayer->getIsHuman() && myKingDanger()) {
				// king will be in danger, undo the move
				undoPlayerMove();
                throw std::invalid_argument{"Your king will be doomed. (en passant)"};
                return;
			} else {
				// move success switch side
				auto nextPlayer = findOpponent();
				currentPlayer = nextPlayer;
			}
			// en passant check & move end
		} else {
			// check for normal pawn move
			if (normalMoveCheck(chessMoved, startSpot, endSpot)) {
				// move success, switch side
				auto nextPlayer = findOpponent();
				currentPlayer = nextPlayer;
			}
		}
        // Pawn check/move finish
    } else if (ChessPieceType::KING == chessMoved->getPieceType()) {
		// check King
		// check validity
		if (currentPlayer->getIsHuman() && !chessMoved->validMove(startSpot, endSpot, board)) {
            throw std::invalid_argument{"this is not a legal move (King)"};
            return;
		}
		// since the king's validmove() will check for whether or nor it put itself under check, so 
		// from here below, the attempt move is always valid. just need to check for castling move
		// attempt move
		auto attempMove = std::make_shared<Move>(chessMoved, startSpot, endSpot);
		if (attempMove->isKingMoveTwoCol()) {
			// castling move
			// check Rook has moved or not
			if (currentPlayer->getIsHuman()) {
				// identify direction of king's move
				if (attempMove->getKingDirection() > 0) {
					// move to left, use the leftmost rook
					auto rook = board->getSpot(startRow, 0)->getPiece();
					if (rook == nullptr || rook->getHasMoved()) {
						// rook doesn't exist or rook already moved
						throw std::invalid_argument{ "invalid castling (Rook)" };
						return;
					}
				} else {
					// move to right, use the rightmost rook
					auto rook = board->getSpot(startRow, 7)->getPiece();
					if (rook == nullptr || rook->getHasMoved()) {
						// rook doesn't exist or rook already moved
						throw std::invalid_argument{ "invalid castling (Rook)" };
						return;
					}
				}
			}
			// castling condition satisfied, do the move
			auto rookMove = attempMove->moveCastle(board);
			moves.push_back(attempMove);
			moves.push_back(rookMove);
			// change player's field
			currentPlayer->setHasCastle(true);			
		} else { // normal move
			attempMove->movePiece();
			moves.push_back(attempMove);
		}
		// move success switch side
		auto nextPlayer = findOpponent();
		currentPlayer = nextPlayer;
		return;
		// King check/move finished
	} else {
		// check for the rest normal move
		if (normalMoveCheck(chessMoved, startSpot, endSpot)) {
			// move success switch side
			auto nextPlayer = findOpponent();
			currentPlayer = nextPlayer;
		}
	}
	// after the move, check for check, checkmate and stalemate
	if (board->isStalemate(currentPlayer->getSide())) {
		status = GameStatus::STALEMATE;
		return;
	}
	if (board->isCheckmate(Colour::BLACK)) {
		status = GameStatus::WHITE_WIN;
		return;
	}
	if (board->isCheckmate(Colour::WHITE)) {
		status = GameStatus::BLACK_WIN;
		return;
	}
}

bool Game::isActive() const {
	if (this->status == GameStatus::ACTIVE) {
		return true;
	}
	return false;
}

bool Game::setupDone() const {
	if (this->board != nullptr) {
		return this->board->done();
	} else {
		// throw an error
	}
	return false;
}

// create an empty board
void Game::setupMode() {
	this->createEmptyBoard();
}


void Game::startGame(std::string whitePlayer, std::string blackPlayer) {
	// 1. creates the player and adds them to the vector
	// 2. changes the game status to active
	// 3. makes a board if a board is nullptr
	if (this->board->isEmpty()) {
		this->board->addDefaultPieces();
	}
	// makes white player
	bool white;
	bool black;
	int blackLevel = 0;
	int whiteLevel = 0;

	// detecting human players
	if (whitePlayer == "human") {
		white = true; 
	}
	if (blackPlayer == "human") {
		black = true;
	}
	// detecting computer players
	if (whitePlayer == "computer[1]") {
		whiteLevel = 1;
		white = false;
	} else if (whitePlayer == "computer[2]") {
		whiteLevel = 2;
		white = false;
	} else if (whitePlayer == "computer[3]") {
		whiteLevel = 3;
		white = false;
	} else if (whitePlayer == "computer[4]") {
		whiteLevel = 4;
		white = false;
	} else {
		// throw invalid player argument
	}

	if (blackPlayer == "computer[1]") {
		blackLevel = 1;
		black = false;
	} else if (blackPlayer == "computer[2]") {
		blackLevel = 2;
		black = false;
	} else if (blackPlayer == "computer[3]") {
		blackLevel = 3;
		black  = false;
	} else if (blackPlayer == "computer[4]") {
		blackLevel = 4;
		black = false;
	} else {
		// throw invalid player argument
	}

	// creating white player
	addPlayer(white, Colour::WHITE, whiteLevel);
	//creating black player
	addPlayer(black, Colour::BLACK, blackLevel);

	// changing the status of the game to active
	this->status = GameStatus::ACTIVE;

	// set the current player
	if (this->firstTurn == Colour::WHITE) {
		this->currentPlayer = players.at(0);
	} else if (this->firstTurn == Colour::BLACK) {
		this->currentPlayer = players.at(1);
	}
}

bool Game::isFinished() const {
	if (this->status == GameStatus::BLACK_WIN || this->status == GameStatus::WHITE_WIN || this->status == GameStatus::STALEMATE) {
		return true;
	} 
	return false;
}

bool Game::isCheck(Colour colour) {
	return board->isCheck(colour);
}

bool Game::isCheckmate(Colour colour) {
	return board->isCheckmate(colour);
}

bool Game::isStalemate(Colour colour) {
	return board->isStalemate(colour);
}


// creates an empty board given that Board is a nullptr
void Game::createEmptyBoard() {
	if (this->board == nullptr) {
		this->board = std::make_shared<Board>(8, 8, true);
	}		
}

bool Game::isValidCharOfPromotionPiece(char ch) {
	if (ch == 'q' || ch == 'Q' || ch == 'r' || ch == 'R' ||
		ch == 'b' || ch == 'B' || ch == 'n' || ch == 'N') {
        return true;
    }
    return false;
}

// check is in check
bool Game::isMyKingCheck(std::shared_ptr<Spot> mykingSpot, std::shared_ptr<Player> opponent) {
    for (auto chess : opponent->getPlayerPieces()) {
        if (chess->getIsValidPiece()) {
            auto oppoStartSpot = board->getSpot(chess->getRow(), chess->getCol());
            if (chess->validMove(oppoStartSpot, mykingSpot, board)) {

                return true;
            }
        }
    }
    return false;
}


std::shared_ptr<Player> Game::findOpponent() {
    for (auto pp : players)
    {
        if (pp->getSide() != currentPlayer->getSide())
        {
            return pp;
        }
    }
    throw std::invalid_argument { "no opponent found" };
    return nullptr;
}

// wrapper for king is doomed
bool Game::myKingDanger()
{
	auto myKing = currentPlayer->getKing();
	auto myKingSpot = board->getSpot(myKing->getRow(), myKing->getCol());
		auto oppo = findOpponent();
    if (oppo == nullptr) {
        throw std::invalid_argument{"where is your opponent"};
        return false;
	}
	return isMyKingCheck(myKingSpot, oppo);
}

bool Game::normalMoveCheck(std::shared_ptr<ChessPiece> chessMoved,
                           std::shared_ptr<Spot> startSpot, std::shared_ptr<Spot> endSpot) {

	// check movable
	if (!chessMoved->validMove(startSpot, endSpot, board))
    {
        throw std::invalid_argument{"Invalid Move (normal)"};
        return false;
    }
	
    // make attempted move
    auto attempMove = std::make_shared<Move>(chessMoved, startSpot, endSpot);
    moves.push_back(attempMove);
    attempMove->movePiece();
    // check king doomed
    if (myKingDanger())
    {
        undoPlayerMove();
        throw std::invalid_argument{"king will be in check (normal)"};
        return false;
	}
	return true;
}

bool Game::stillMoved(std::shared_ptr<ChessPiece> lastChess, std::vector<std::shared_ptr<Move>> moves) {
    for (auto m : moves) {
        if (m->getPieceMoved() == lastChess) {
            return true;
            break;
        }
    }
    return false;
}
