#ifndef _GAMESTATUS_H
#define _GAMESTATUS_H

enum class GameStatus {
	ACTIVE,
	INACTIVE,
	BLACK_WIN,
	WHITE_WIN,
	STALEMATE,
	RESIGNATION
};

#endif

