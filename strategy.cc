#include "strategy.h"
#include "board.h"
#include "chessPiece.h"
#include "spot.h"
#include "move.h"
#include "pawn.h"

// Strategy::calculateMove(board, colour, allMoves) calculates a random valid move 
//   this is a base implementation of a virtual method, useable by all derived classes
std::vector<int> Strategy::calculateMove(std::shared_ptr<Board> board, Colour colour,std::vector<std::shared_ptr<Move>> allMoves) {
 
    // by default, this method calculates a random valid move
    std::vector<std::shared_ptr<ChessPiece>> pieces;
    if (colour == Colour::WHITE) {
        pieces = board->getWhitePiece(); // returns a reference (DO NOT MUTATE PIECES!)
    }
    else if (colour == Colour::BLACK) {
        pieces = board->getBlackPiece(); // returns a reference (DO NOT MUTATE PIECES!)
    }

    std::vector<int> choices{}; // for control flow below
    int numPieces = pieces.size();
    for (int i = 0; i < numPieces; ++i) {
        choices.push_back(i);
    }

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(choices.begin(), choices.end(), g); // shuffle our choices of an index from our pieces
    auto chessPiece = pieces.at(choices.back()); // choose a random index from pieces
    choices.pop_back();

    while (!chessPiece->getIsValidPiece()) { // find a different, valid piece, to move
        if (choices.empty()) throw std::logic_error{"The computer has no valid move choices."};
        chessPiece = pieces.at(choices.back());
        choices.pop_back();
    }
    
    auto moveInformation = chessPiece->randomValidMove(board->getSpot(chessPiece->getRow(), chessPiece->getCol()), board);
    while (moveInformation.empty() || !chessPiece->getIsValidPiece()) {
        while (!chessPiece->getIsValidPiece()) { // find a different, valid piece, to move
            if (choices.empty()) throw std::logic_error{"The computer has no valid move choices."};
            chessPiece = pieces.at(choices.back());
            choices.pop_back();
        }
        moveInformation = chessPiece->randomValidMove(board->getSpot(chessPiece->getRow(), chessPiece->getCol()), board);
        chessPiece = pieces.at(choices.back());
        choices.pop_back();
    }
    return moveInformation;


}

std::vector<moveSpot> Strategy::killMoves(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves) {
    std::vector<std::shared_ptr<ChessPiece>> myPieces;
    std::vector<std::shared_ptr<ChessPiece>> oppoPieces;
    std::vector<moveSpot> killmove;
    if (colour == Colour::WHITE) {
        myPieces = board->getWhitePiece();
        oppoPieces = board->getBlackPiece();
    }
    else if (colour == Colour::BLACK) {
        myPieces = board->getBlackPiece();
        oppoPieces = board->getWhitePiece();
    }
    // check availability for enpassant
    bool canEnPassant = false;
    if (allMoves.size() != 0 && allMoves.back()->isPawnMoveTwo()) {
        canEnPassant = true;
    }
    // loop to get all killing move
    // outer loop is oppo, inner loop is mine, use mine to capture oppo
    for (auto oppoChess : oppoPieces) {
        if (oppoChess->getIsValidPiece()) {
            auto finalSpot = board->getSpot(oppoChess->getRow(), oppoChess->getCol());
            if (finalSpot->isOccupied()) { // double check the spot is occupied
                int level = setKillLevel(oppoChess);
                for (auto myChess : myPieces) {
                    if (myChess->getIsValidPiece()) {
                        auto beginSpot = board->getSpot(myChess->getRow(), myChess->getCol());
                        // add kiling own king check
                        if (myChess->validMove(beginSpot, finalSpot, board) && !board->isCheck(beginSpot, finalSpot, colour)) {
                            // distinguish promotion kill
                            if (myChess->getPieceType() == ChessPieceType::PAWN &&
                                (finalSpot->getRow() == 0 || finalSpot->getRow() == 7)) {
                                moveSpot add{ level, beginSpot, finalSpot, true };
                                killmove.push_back(add);
                            }
                            else {
                                moveSpot add{ level, beginSpot, finalSpot, false };
                                killmove.push_back(add);
                            }
                        }
                        // add possible en passant move
                        addEnPassantKill(canEnPassant, myChess, oppoChess, board, allMoves, killmove);
                    }
                }
            }
        }
    }
    return killmove;
}

void Strategy::addEnPassantKill(bool canEnPassant, std::shared_ptr<ChessPiece> myChess,
    std::shared_ptr<ChessPiece> oppoChess, std::shared_ptr<Board> board,
    std::vector<std::shared_ptr<Move>> allMoves, std::vector<moveSpot>& killmove) {
    if (canEnPassant && myChess->getPieceType() == ChessPieceType::PAWN &&
        allMoves.back()->getPieceMoved() == oppoChess && oppoChess->getPieceType() == ChessPieceType::PAWN) {
        // if valid enpassant, calculate enpassant move
        std::shared_ptr<Move> lastAction = allMoves.back();
        int oppoDirection = lastAction->getDirection();
        int oppoRow = oppoChess->getRow();
        int oppoCol = oppoChess->getCol();
        std::shared_ptr<Spot> targetSpot;

        if (oppoDirection > 0) {
            // if direction is positive, opponent move downward, so my piece move upward
            targetSpot = board->getSpot(oppoRow + 1, oppoCol);
        }
        else {
            // negative direction, oppo moves upward, i move downward
            targetSpot = board->getSpot(oppoRow - 1, oppoCol);
        }
        auto beginSpot = board->getSpot(myChess->getRow(), myChess->getCol());
        // check validity in case
        if (static_cast<Pawn*>(&*myChess)->validMove(true, beginSpot, targetSpot, board) &&
            !board->isCheck(beginSpot, targetSpot, myChess->getColour())) {
            moveSpot add{ 1, beginSpot, targetSpot, false };
            killmove.push_back(add);
        }
    }
}

int Strategy::setKillLevel(std::shared_ptr<ChessPiece> chess) {
    if (chess->getPieceType() == ChessPieceType::PAWN) {
        return 1;
    }
    else if (chess->getPieceType() == ChessPieceType::KING) {
        return 10;
    }
    else if (chess->getPieceType() == ChessPieceType::QUEEN) {
        return 9;
    }
    else if (chess->getPieceType() == ChessPieceType::KNIGHT) {
        return 6;
    }
    else {
        return 8; // bishop and rook
    }
}





