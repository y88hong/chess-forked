#include "chessPiece.h"
#include "board.h"
#include <random>
#include "spot.h"
#include "colour.h"
#include "chessPieceType.h"

ChessPiece::ChessPiece(ChessPieceType name, Colour colour, int row, int col):
    name{name}, colour{colour}, hasMoved{false}, isValidPiece{true}, row{row}, col{col} 
{ }
ChessPiece::ChessPiece(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col):
    name{name}, colour{colour}, hasMoved{hasMoved}, isValidPiece{isValidPiece}, row{row}, col{col} 
{ }

// == Accessors and Mutators
ChessPieceType ChessPiece::getPieceType() const { return name; }

Colour ChessPiece::getColour() const { return colour; }

bool ChessPiece::getHasMoved() const { return hasMoved; }

void ChessPiece::setHasMoved(bool hasMoved) { 
    this->hasMoved = hasMoved; 
}

bool ChessPiece::getIsValidPiece() const { return isValidPiece; }

void ChessPiece::setIsValidPiece(bool isValidPiece) {
    this->isValidPiece = isValidPiece;
}

bool ChessPiece::isColour(Colour colour) const { return this->colour == colour; }

int ChessPiece::getRow() const { return row; }

int ChessPiece::getCol() const { return col; }

void ChessPiece::setRow(int row) {
    this->row = row;
}

void ChessPiece::setCol(int col) {
    this->col = col;
}

// == end Accessors/Mutators

char ChessPiece::pieceTypeAsChar() const {
    char ASCIIshift = colour == Colour::BLACK ? 'a' - 'A' : 0;

    if (name == ChessPieceType::KING) return 'K' + ASCIIshift;
    else if (name == ChessPieceType::QUEEN) return 'Q' + ASCIIshift;
    else if (name == ChessPieceType::BISHOP) return 'B' + ASCIIshift;
    else if (name == ChessPieceType::KNIGHT) return 'N' + ASCIIshift;
    else if (name == ChessPieceType::ROOK) return 'R' + ASCIIshift;
    return 'P' + ASCIIshift;
}

// ChessPiece::isValidCapture(Spot *, Spot *) returns if a given move is a valid capture
// "capture" here either means the end spot is empty, OR
//   the start spot piece has a different colour than the end spot piece
bool ChessPiece::isValidCapture(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end) {
    std::shared_ptr<ChessPiece> startP = start->getPiece();
    std::shared_ptr<ChessPiece> endP = end->getPiece();
    if (startP == nullptr) return false;
    return (endP == nullptr) || (startP->colour != endP->colour);
}

std::ostream& operator<<(std::ostream &os, const ChessPiece &piece) {
    os << piece.pieceTypeAsChar();
    return os;
}
