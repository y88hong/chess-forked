#ifndef __KING__H
#define __KING__H

#include "chessPiece.h"

enum class ChessPieceType;
enum class Colour;
class Spot;
class Board;

class King: public ChessPiece {
    public:
    King(ChessPieceType name, Colour colour, int row, int col);
    King(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col);

    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck = true) override;
    bool validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck = true) override;
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, Board *board) override;
    std::vector<int> randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) override;
    
    private:
    bool isValidCastle(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, int hmove);
    bool isValidCastle(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, int hmove);
};

#endif

