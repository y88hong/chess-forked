#include "subject.h"

Subject::Subject() {}
Subject::~Subject() {}

void Subject::attach(Observer *o)
{
    observers.push_back(o);
}

void Subject::detach(Observer *o)
{
    for (auto it = observers.begin(); it != observers.end(); ++it)

    {
        if (*it == o) // *it is unique_ptr
        {
            observers.erase(it);
            break;
        }
    }
}

void Subject::notifyObservers() {
    for (std::size_t i = 0; i < observers.size(); i++) {
        observers.at(i)->notify();
    }
}

