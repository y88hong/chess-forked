#ifndef _CHESSPIECETYPE_H
#define _CHESSPIECETYPE_H

enum class ChessPieceType {
    PAWN,
    ROOK,
    BISHOP,
    KNIGHT,
    QUEEN,
    KING
};

#endif

