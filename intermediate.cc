#include "intermediate.h"
#include "board.h"
#include "chessPiece.h"
#include "spot.h"
#include "move.h"


// do we need to change a name for that
std::vector<int> Intermediate::calculateMove(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves) {
    std::vector<moveSpot> moves = killMoves(board, colour, allMoves);
    if (!moves.empty()) {
        moveSpot mostValue{ 0, nullptr, nullptr, false };
        for (auto m : moves) {
            if (m.killLevel > mostValue.killLevel) {
                mostValue = m;
            }
        }
        // startrow, startcol, endrow, endcol
        // check if it's promotion
        if (mostValue.promoteKill) {
            return std::vector<int>{ mostValue.start->getRow(), mostValue.start->getCol(), mostValue.end->getRow(), mostValue.end->getCol(), 'Q'};
        } else {
            return std::vector<int>{ mostValue.start->getRow(), mostValue.start->getCol(), mostValue.end->getRow(), mostValue.end->getCol()};       
        }
    } else {
        // call beginner if kill move is empty
        return Strategy::calculateMove(board, colour, allMoves);
    }
}

