#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include "game.h"
#include "gamestatus.h"
#include "colour.h"
#include "textobserver.h"
#include "graphicsobserver.h"
#include "displayboard.h"
#include "board.h"
using namespace std;

const char LEFTMOST_COL = 'a';
const char RIGHTMOST_COL = 'h';
const int LOWEST_ROW = 1;
const int HIGHEST_ROW = 8;
const int BOARD_WIDTH = 8;

// == for input "conversion"

// throwing function
bool isValidCharOfPromotionPiece(char ch) {
    if (ch == '\0' || ch == 'k' || ch == 'K' || ch == 'q' || ch == 'Q' || 
        ch == 'r' || ch == 'R' || ch == 'b' || ch == 'B' ||
        ch == 'n' || ch == 'N') {
            return true;
        }
    throw std::invalid_argument{"Invalid 'piece to promote to' provided."};
    return false;
}

// throwing function
int rowToInt(int row) {
    if (LOWEST_ROW <= row && row <= HIGHEST_ROW) {
        return row - LOWEST_ROW;
    } else {
        throw std::invalid_argument{"Invalid row provided."};
        return -1;
    }
}

// throwing function
int charColToInt(char col) {
    if (LEFTMOST_COL <= col && col <= RIGHTMOST_COL) {
        return col - LEFTMOST_COL;
    } else {
        throw std::invalid_argument{"Invalid column provided."};
        return -1;
    }
}

void display(std::shared_ptr<DisplayBoard> &displayBoard) {
    displayBoard->notifyObservers();
}

void printWins(int whiteWins, int blackWins) {
    std::cout << "White wins: " << whiteWins << std::endl;
    std::cout << "Black wins: " << blackWins << std::endl;
}

// == Command Handler
int main () {
  auto allGames = std::vector<shared_ptr<Game>>();
  int whiteWins = 0;
  int blackWins = 0;
 // outer loop - to play consecutive games
 while (cin.peek() != EOF) {
    auto game = std::make_shared<Game>();
    allGames.push_back(game);
    std::vector<std::shared_ptr<Observer>> gameObservers;
    std::shared_ptr<DisplayBoard> displayBoard = std::make_shared<DisplayBoard>(game->getBoard());
    gameObservers.emplace_back(std::make_shared<TextObserver>(displayBoard));
  cout << "== Welcome to our Chess Program! ==" << endl;
  while (true) { // inner loop to play a single game (until resign/EOF)
    try {
        std::string input;
        getline(cin, input);
        istringstream iss{input};
        string cmd;
        iss >> cmd;

        if (cmd == "game") {
            if (game->isActive()) {
                throw std::invalid_argument{"There is already an ongoing game!"};
            }
            string player1, player2;
            iss >> player1;
            iss >> player2;
            game->startGame(player1, player2); // sets game to active
            display(displayBoard);

        } else if (cmd == "resign") {
            game->resign();
            break;
        } else if (cmd == "move") {
            if (!game->isActive()) {
                throw std::invalid_argument{"There is no ongoing game."};
            }
            // check if it's a computer move
            if (iss.eof()) { // eof indicates command was simply 'move'
                game->playerMove(); // will use default parameters
                display(displayBoard);
                continue;
            }

            int startRow, endRow;
            char startCol, endCol;

            // expected input style: e1 c3
            iss >> startCol;
            iss >> startRow;
            iss >> endCol;
            iss >> endRow;

            char promotionPiece = '\0';
            iss >> promotionPiece;
            isValidCharOfPromotionPiece(promotionPiece); // may throw
            game->playerMove(rowToInt(startRow), charColToInt(startCol), rowToInt(endRow), charColToInt(endCol), promotionPiece);
            display(displayBoard);
        } else if (cmd == "setup") {
            if (game->isActive()) {
                throw std::invalid_argument{"Cannot enter setup mode during an active game."};
            }
	        std::cout << "== You have entered setup mode." << std::endl;
            game->setupMode();
            display(displayBoard);
            while (true) {
                try {
                    std::string setupInput;
                    getline(cin, setupInput);
                    istringstream iss{setupInput};
                    string setupcmd;
                    iss >> setupcmd;

                    if (setupcmd == "+") {
                        char piece;
                        iss >> piece;

                        char col;
                        int row;
                        iss >> col;
                        iss >> row;
                        game->addPiece(rowToInt(row), charColToInt(col), piece);
			            display(displayBoard);
                    } else if (setupcmd == "-") {
                        char col;
                        int row;
                        iss >> col;
                        iss >> row;
                        game->removePiece(rowToInt(row), charColToInt(col));
			            display(displayBoard);
                    } else if (setupcmd == "=") {
                        string colour;
                        iss >> colour;
                        for (auto &p: colour) {
		                    p = std::tolower(p);
	                    }
                        colour[0] = std::toupper(colour[0]);
                        game->changeTurn(colour); // player's turn
                        std::cout << colour << " is now moving next." << std::endl;
                    } else if (setupcmd == "display") {
                        display(displayBoard);
                    } else if (setupcmd == "done") {
                        if (game->setupDone()) {
                            std::cout << "Final state of board..." << std::endl;
                            display(displayBoard);
                            std::cout << "== You have successfully exited setup mode." << std::endl;
                            std::cout << "==  Remember you cannot enter this mode when game is active." << std::endl;
                            break;
                        } else {
                            throw std::invalid_argument{"This board is invalid."};
                        }
                    } else if (setupcmd == "") {
                        cerr << "Aborting setup mode early." << endl;
                        break;
                    } else {
                        throw std::invalid_argument{"Invalid setup mode command provided."};
                    }
                    
                } catch (std::exception &e) {
                    cerr << "Error caught in setup mode: " << e.what() << endl;
                    cerr << "Continuing setup..." << endl;
                }
            } 
        } else if (cmd == "undo") {
            game->undoMove();
            display(displayBoard);
        } else if (cmd == "show_graphics") {
            gameObservers.emplace_back(std::make_shared<GraphicsObserver>(displayBoard));
        } else if (cmd == "display") {
            display(displayBoard);
        } else if (cmd == "") {
            cerr << "End of file reached. Exiting current game early." << endl;
            break;
        } else {
            throw std::invalid_argument{"Command '" + cmd + "' is unsupported."};
        }
        if (game->isFinished()) {
            break;
        }
        if (game->isCheck(Colour::BLACK)) {
            std::cout << "Black is in check." << std::endl;
        } else if (game->isCheck(Colour::WHITE)) {
            std::cout << "White is in check." << std::endl;
        }
    } catch (std::exception &e) {
        if (game->isFinished()) {
            break;
        }
        cerr << "Exception occurred with message: " << e.what() << endl;
        cerr << "Continuing ..." << endl;
    }
   } // end current game, current command
    if (game->getStatus() == GameStatus::STALEMATE) {
		std::cout << "Stalemate reached." << std::endl;
	} else if (game->isCheckmate(Colour::BLACK)) {
		std::cout << "Checkmate! White wins!" << std::endl;
	} else if (game->isCheckmate(Colour::WHITE)) {
		std::cout << "Checkmate! Black wins!" << std::endl;
	}
    if (game->getStatus() == GameStatus::WHITE_WIN) {
        ++whiteWins;
    } else if (game->getStatus() == GameStatus::BLACK_WIN) {
        ++blackWins;
    }
   cout << "== Current game has finished. Displaying win counter." << endl;
   cout << endl;
   printWins(whiteWins, blackWins);
   cout << endl;
  } // end outer loop (games tracker)
}
