#ifndef __CHESSSTRATEGY__H
#define __CHESSSTRATEGY__H

#include <vector>
#include <memory>
#include "colour.h"

class Spot;
class Board;
class ChessPiece;
class Move;

struct moveSpot {
    int killLevel;
    std::shared_ptr<Spot> start;
	std::shared_ptr<Spot> end;
	bool promoteKill;
};

class Strategy {
public:
		// calculate coordinates of a move
		virtual std::vector<int> calculateMove(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves) = 0;
		// return start and end spot of move with capture
		std::vector<moveSpot> killMoves(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves);
		// set the levels of different pieces
		int setKillLevel(std::shared_ptr<ChessPiece> chess);
		// detect possible en passant move (for Intermediate and Expert)
		void addEnPassantKill(bool canEnPassant, std::shared_ptr<ChessPiece> myChess,
		std::shared_ptr<ChessPiece> oppoChess, std::shared_ptr<Board> board,
		std::vector<std::shared_ptr<Move>> allMoves, std::vector<moveSpot>& killmove);
};
#endif

