#ifndef _GRAPHICSOBSERVER_H
#define _GRAPHICSOBSERVER_H
#include "observer.h"
#include "displayboard.h"
#include <memory>

class Xwindow;

class GraphicsObserver: public Observer {
	std::shared_ptr<DisplayBoard> subject;
	std::shared_ptr<Xwindow> display;
	int boardHeight;
	int boardWidth;

	public:
	GraphicsObserver(std::shared_ptr<DisplayBoard> subject, int boardHeight = 8, int boardWidth = 8);
	void notify() override;
	~GraphicsObserver();
};

#endif
