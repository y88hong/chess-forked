#ifndef __BEGINNERSTRATEGY_H
#define __BEGINNERSTRATEGY_H

#include <vector>
#include <memory>
#include "strategy.h"
#include "colour.h"

class Beginner : public Strategy {
    public:
    std::vector<int> calculateMove(std::shared_ptr<Board> board, Colour colour,std::vector<std::shared_ptr<Move>> allMoves) override;
};

#endif

