#include "knight.h"
#include "spot.h"
#include "board.h"

Knight::Knight(ChessPieceType name, Colour colour, int row, int col):
    ChessPiece(name, colour, row, col)
{ }

Knight::Knight(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col):
    ChessPiece(name, colour, hasMoved, isValidPiece, row, col)
{ }
    
bool Knight::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck) {
    return validMove(start, end, &*board, checkCheck);
}

bool Knight::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck) {
    if (start == nullptr || end == nullptr || start == end) return false;
    
    int startRow = start->getRow();
    int startCol = start->getCol();
    int endRow = end->getRow();
    int endCol = end->getCol();
    
    // brute force loop through all 8 of a knight's possible moves
    if (startCol + 2 == endCol || startCol - 2 == endCol) {
        if (startRow + 1 == endRow || startRow - 1 == endRow) { 
            return ChessPiece::isValidCapture(start, end) && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour())); 
        }
    } else if (startCol + 1 == endCol || startCol - 1 == endCol) {
        if (startRow + 2 == endRow || startRow - 2 == endRow) { 
            return ChessPiece::isValidCapture(start, end) && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour())); 
        }
    }
    return false;
}

std::vector<int> Knight::randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) {
    return randomValidMove(start, &*board);
}

std::vector<int> Knight::randomValidMove(std::shared_ptr<Spot> start, Board *board) {
    // random number generator courtesy of https://en.cppreference.com/w/cpp/numeric/random
    std::vector<int> retval{};
    std::vector<int> choices{}; // for control flow below
    for (int i = 1; i <= 8; ++i) {
        choices.push_back(i);
    }
    int endCol = -1;
    int endRow = -1;

    std::random_shuffle(choices.begin(), choices.end());
    
    while (!choices.empty()) {
        int startCol = start->getCol();
        int startRow = start->getRow();
        int hdir = 0;
        int vdir = 0;
        int choice = choices.back();
        choices.pop_back();
        // brute force choose random Knight move
        if (choice == 1) {
            hdir = 2;
            vdir = 1;
        } else if (choice == 2) {
            hdir = -2;
            vdir = 1;
        } else if (choice == 3) {
            hdir = 2;
            vdir = -1;
        } else if (choice == 4) {
            hdir = -2;
            vdir = -1;
        } else if (choice == 5) {
            hdir = 1;
            vdir = 2;
        } else if (choice == 6) {
            hdir = -1;
            vdir = 2;
        } else if (choice == 7) {
            hdir = 1;
            vdir = -2;
        } else {
            hdir = -1;
            vdir = -2;
        }
        endRow = startRow + vdir;
        endCol = startCol + hdir;
        if (board->isValidCoordinate(endRow, endCol) && validMove(start, board->getSpot(endRow, endCol), board)) {
            retval.emplace_back(startRow);
            retval.emplace_back(startCol);
            retval.emplace_back(endRow);    
            retval.emplace_back(endCol);   
            break;
        }
    }
    return retval;
}
