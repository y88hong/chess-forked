#include "spot.h"
#include "board.h"
#include "pawn.h"

Pawn::Pawn(ChessPieceType name, Colour colour, int row, int col):
    ChessPiece(name, colour, row, col)
{ }

Pawn::Pawn(ChessPieceType name, Colour colour, bool hasMoved, bool isValidPiece, int row, int col):
    ChessPiece(name, colour, hasMoved, isValidPiece, row, col)
{ }

bool Pawn::isValidPawnCapture(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, bool isEnPassantMove) {
    std::shared_ptr<ChessPiece> startP = start->getPiece();
    std::shared_ptr<ChessPiece> endP = end->getPiece();
    if (startP == nullptr) return false;
    if (isEnPassantMove && endP == nullptr) {
        return true;
    }
    return endP && (startP->getColour() != endP->getColour());
}

bool Pawn::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck)  {
    return validMove(start, end, &*board, checkCheck);
}

bool Pawn::validMove(std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck) {
    return validMove(false, start, end, board, checkCheck);
}

// extra enPassant field is
bool Pawn::validMove(bool isEnPassantMove, std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, std::shared_ptr<Board> board, bool checkCheck) {
    return validMove(isEnPassantMove, start, end, &*board, checkCheck);
}

bool Pawn::validMove(bool isEnPassantMove, std::shared_ptr<Spot> start, std::shared_ptr<Spot> end, Board *board, bool checkCheck) {

    if (start == nullptr || end == nullptr || start == end) return false;
    
    int startRow = start->getRow();
    int startCol = start->getCol();
    int endRow = end->getRow();
    int endCol = end->getCol();

    int dir = 1;
    if (ChessPiece::getColour() == Colour::BLACK) {
        dir = -1;
    }

    if (startCol != endCol) {
        if ((startCol + 1 == endCol || startCol - 1 == endCol) && startRow + dir == endRow) {
            return isValidPawnCapture(start, end, isEnPassantMove) && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour()));
        }
        return false;
    }

    // assume now column doesn't change - MUST be a vertical move
    // return if vert. move is one square OR two squares
    if ((startRow + dir == endRow) || (!ChessPiece::getHasMoved() && (startRow + (dir * 2) == endRow))) {
        return !board->getSpot(startRow + dir, startCol)->isOccupied() && 
               !end->isOccupied() && (!checkCheck || !board->isCheck(start, end, ChessPiece::getColour()));
    }
    return false;
}

char Pawn::getRandomPieceAsChar() const {
    std::random_device r;
    std::default_random_engine eng1(r());
    std::uniform_int_distribution<int> randNumGenerator(1, 4);
    int randNum = randNumGenerator(eng1);
    char retval;
    char ASCIIshift = ChessPiece::getColour() == Colour::BLACK ? 'a' - 'A' : 0;
    if (randNum == 1) {
        retval = 'Q' + ASCIIshift;
    } else if (randNum == 2) {
        retval = 'B' + ASCIIshift;
    } else if (randNum == 3) {
        retval = 'N' + ASCIIshift;
    } else {
        retval = 'R' + ASCIIshift;
    }
    return retval;
}

std::vector<int> Pawn::randomValidMove(std::shared_ptr<Spot> start, std::shared_ptr<Board> board) {
    return randomValidMove(start, &*board);
}

// random move DOES NOT include the potentialy to capture en passant
std::vector<int> Pawn::randomValidMove(std::shared_ptr<Spot> start, Board *board) {
    // random number generator courtesy of https://en.cppreference.com/w/cpp/numeric/random
    // brute force check all four possible pawn moves (two captures, two fwd. movements)
    std::vector<int> retval{};
    std::vector<int> choices{}; // for control flow below
    for (int i = 1; i <= 4; ++i) {
        choices.push_back(i);
    }
    int startCol = start->getCol();
    int startRow = start->getRow();
    int dir = ChessPiece::getColour() == Colour::WHITE ? 1 : -1;
    int endCol = -1;
    int endRow = -1;

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(choices.begin(), choices.end(), g);

    while (!board->isValidCoordinate(endRow, endCol) || !validMove(start, board->getSpot(endRow, endCol), board)) {
        if (choices.empty()) return retval;
        int choice = choices.back();
        choices.pop_back();
        int hdir = 0;
        int vdir = 0;
        // 1 = try to move fwd. once
        // 2 = try to move fwd. twice
        // 3 = try to capture (left)
        // 4 = try to capture (right)
        if (choice == 1) {
            vdir = dir * 1;
        } else if (choice == 2) {
            vdir = dir * 2;
        } else if (choice == 3) {
            hdir = 1;
            vdir = 1;
        } else {
            hdir = -1;
            vdir = 1;
        }
        endRow = startRow + vdir;
        endCol = startCol + hdir;
    }

    retval.emplace_back(startRow);
    retval.emplace_back(startCol);
    retval.emplace_back(endRow);    
    retval.emplace_back(endCol);

    // handling pawn promotions
    if (dir > 0 && endRow == board->getHeight() - 1) {
        retval.emplace_back(getRandomPieceAsChar());
    } else if (dir < 0 && endRow == 0) { // bottom of board
        retval.emplace_back(getRandomPieceAsChar());
    }
    return retval;
}

