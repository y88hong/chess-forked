#include "spot.h"
#include "chessPiece.h"

/**
 * Spot(row, col, spotColour, piece) creates a spot on the board at the given 
 * 	row and col, with the given spotColour and piece to the placed on that
 * 	spot
 * Note: default parameters are set for piece to be nullptr
 * 	 default parameter for spotColour is set to be white
 */
Spot::Spot(int row, int col, Colour spotColour, std::shared_ptr<ChessPiece>  piece):
	row{row}, col{col}, spotColour{spotColour}, piece{piece}
{}

/**
 * getRow() returns the row or Y position 
 * 	of the spot on the board
 */ 
int Spot::getRow() const {
	return this->row;
}

/**
 * getCol() returns the col or X position
 * 	of the spot on the board
 */ 
int Spot::getCol() const {
	return this->col;
}

/**
 * getSpotColour() returns the colour of the spot
 * Note: colour is either black or white
 */
Colour Spot::getSpotColour() const {
	return this->spotColour;
}

/**
 * getPiece() returns the piece on the spot
 * Note: returns nullptr if there is no piece on the spot
 */
std::shared_ptr<ChessPiece> Spot::getPiece() const {
	return this->piece;
}

/**
 * setPiece(piece) places the piece on the spot
 * Note: if there is already a piece on the spot, then
 * 	 that piece is replaced 
 */
void Spot::setPiece(std::shared_ptr<ChessPiece> piece) {
	if (this->piece == piece) {
		return;
	}
	this->piece = piece;
}

/**
 * removePiece() removes a piece from the spot and returns
 * 	the removed piece
 * Note: if there is no piece on the spot, then return 
 * 	 nullptr
 */
std::shared_ptr<ChessPiece> Spot::removePiece() {
	if (this->piece == nullptr) {
		return nullptr;
	}
	std::shared_ptr<ChessPiece> remove = this->piece;
	this->piece = nullptr;
	return remove;
}

/**
 * isOccupied() checks if a spot is 
 * 	occupied by a piece
 */
bool Spot::isOccupied() const {
	if (this->piece == nullptr) {
		return false;
	}
	return true;
}

/**
 * operator<<(out, spot) overloads the output operator 
 * 	 to output the piece on the given spot
 * Note: 1. For a white unoccupied spot, prints a space
 * 	 2. For a black unoccupied spot, prints an underscore
 * 	 3. Else prints the piece on the spot
 */
std::ostream& operator<<(std::ostream &out, const Spot &spot) {
	if (spot.isOccupied()) {
		// chess piece prints out it piece accordingly
		out << *(spot.getPiece()); 
	} else {
		// space for unoccupied white spot
		if (spot.getSpotColour() == Colour::WHITE) {
			out << " ";
		} 
		// underscore for unoccupied black spot
		if (spot.getSpotColour() == Colour::BLACK) {
			out << "_";
		}
	}
	return out;
}



