#include "expert.h"
#include "strategy.h"
#include "board.h"
#include "chessPiece.h"
#include "spot.h"
#include "move.h"
#include "intermediate.h"
#include <istream>

std::vector<int> Expert::calculateMove(std::shared_ptr<Board> board, Colour colour, std::vector<std::shared_ptr<Move>> allMoves) {
    // identify lists own piece in danger
    // use killmoves to identify all opponent's kill
    Colour oppoColor = (colour == Colour::WHITE) ? Colour::BLACK : Colour::WHITE;
    auto oppoPieces = (oppoColor == Colour::WHITE) ? board->getWhitePiece() : board->getBlackPiece();
    std::vector<moveSpot> pieceInDanger = killMoves(board, oppoColor, allMoves);
    while (!pieceInDanger.empty()) {
        // find most dagerous one
        moveSpot mostValue{ 0, nullptr, nullptr, false };
        size_t mostValueIndex = 0;
        for (size_t i = 0; i < pieceInDanger.size(); i++) {
            if (pieceInDanger.at(i).killLevel > mostValue.killLevel) {
                mostValue = pieceInDanger.at(i);
                mostValueIndex = i;
            }
        }
        auto dangerPiece = mostValue.end->getPiece();
        auto dangerPieceLocation = mostValue.end;

        // kill move of dangerPiece 
        std::vector<moveSpot> killmove;
        // use danger to capture oppo's chess
        for (auto oppoPiece : oppoPieces) {
            if (oppoPiece->getIsValidPiece()) {
                auto endSpot = board->getSpot(oppoPiece->getRow(), oppoPiece->getCol());
                int level = setKillLevel(oppoPiece);
                if (dangerPiece->validMove(dangerPieceLocation, endSpot, board)&&
                    !board->isCheck(dangerPieceLocation, endSpot, colour)) {
                    if (dangerPiece->getPieceType() == ChessPieceType::PAWN &&
                        (endSpot->getRow() == 0 || endSpot->getRow() == 7)) {
                        moveSpot add{ level, dangerPieceLocation, endSpot, true };
                        killmove.push_back(add);
                    }
                    else {
                        moveSpot add{ level, dangerPieceLocation, endSpot, false };
                        killmove.push_back(add);
                    }
                }
                // check enpassant kill
                bool canEnPassant = false;
                if (allMoves.size() != 0 && allMoves.back()->isPawnMoveTwo()) {
                    canEnPassant = true;
                }
                addEnPassantKill(canEnPassant, dangerPiece, oppoPiece, board, allMoves, killmove);
            }
        }
        
        if (!killmove.empty()) {
            return captureMoveTrans(killmove);
        }       

        // if endangered piece can't kill, make a random move
        // if it's king
        if (dangerPiece->getPieceType() == ChessPieceType::KING) {
            return kingSpecial(board, dangerPieceLocation, colour);
        }
        
        auto moveInformation = dangerPiece->randomValidMove(board->getSpot(dangerPiece->getRow(), dangerPiece->getCol()), board);
        // because of seed, can't use loop to check
        if (!moveInformation.empty()) {
            auto randomEnd = board->getSpot(moveInformation.at(2), moveInformation.at(3));
            if (!inDanger(board, dangerPiece, randomEnd, colour)) {
                return moveInformation;
            }
        }
        // not capture nor random move, check next chess piece
        pieceInDanger.erase(pieceInDanger.begin() + mostValueIndex);
    }
    // no danger piece, or danger piece is definitely dead, use strategy 2, then 1
    std::unique_ptr<Strategy> level2 = std::make_unique<Intermediate>();
    return level2->calculateMove(board, colour, allMoves);
}

std::vector<int> Expert::kingSpecial(std::shared_ptr<Board> board, std::shared_ptr<Spot> kingSpot, Colour colour) {
    for (int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
            int newRow = kingSpot->getRow() + y;
            int newCol = kingSpot->getCol() + x;
            if ((newRow >= 0 && newRow <= 7) &&
                (newCol >= 0 && newCol <= 7) &&
                kingSpot->getPiece()->validMove(kingSpot, board->getSpot(newRow, newCol), board) &&
                !inDanger(board, kingSpot->getPiece(), board->getSpot(newRow, newCol), colour)) {
                std::vector<int> result{ kingSpot->getRow(), kingSpot->getCol(), newRow, newCol };
                    return result;
            }
        }
    }
    std::vector<int> result{};
    return result;
}

// in danger only applies for random move
bool Expert::inDanger(std::shared_ptr<Board> board, std::shared_ptr<ChessPiece> dangerPiece, std::shared_ptr<Spot> newEnd, Colour colour) {
    auto oppoPieces = (colour == Colour::WHITE) ? board->getBlackPiece() : board->getWhitePiece();
    auto originalSpot = board->getSpot(dangerPiece->getRow(), dangerPiece->getCol());
    // simple attempted move
    originalSpot->removePiece();
    newEnd->setPiece(dangerPiece);
    int oldRow = dangerPiece->getRow();
    int oldCol = dangerPiece->getCol();
    dangerPiece->setRow(newEnd->getRow());
    dangerPiece->setCol(newEnd->getCol());
    for (auto oppoPiece : oppoPieces) {
        if (oppoPiece->getIsValidPiece()) {
            auto startSpot = board->getSpot(oppoPiece->getRow(), oppoPiece->getCol());
            if (oppoPiece->validMove(startSpot, newEnd, board)) {
                originalSpot->setPiece(dangerPiece);
                dangerPiece->setRow(oldRow);
                dangerPiece->setCol(oldCol);
                newEnd->removePiece();
                return true;
            }            
        }
    }
    originalSpot->setPiece(dangerPiece);
    dangerPiece->setRow(oldRow);
    dangerPiece->setCol(oldCol);
    newEnd->removePiece();
    return false;
}

std::vector<int> Expert::captureMoveTrans(std::vector<moveSpot> moves) {
    moveSpot mostValue{ 0, nullptr, nullptr, false };
    for (auto m : moves) {
        if (m.killLevel > mostValue.killLevel) {
            mostValue = m;
        }
    }
    // startrow, startcol, endrow, endcol
    // check if it's promotion
    if (mostValue.promoteKill) {
        return std::vector<int>{ mostValue.start->getRow(), mostValue.start->getCol(), mostValue.end->getRow(), mostValue.end->getCol(), 'Q'};
    }
    else {
        return std::vector<int>{ mostValue.start->getRow(), mostValue.start->getCol(), mostValue.end->getRow(), mostValue.end->getCol()};
    }
}
