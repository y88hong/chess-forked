CXX = g++
CXXFLAGS = -std=c++14 -Wall -g -MMD -Werror=vla
EXEC=chess
OBJECTS = main.o spot.o board.o chessPiece.o pawn.o king.o queen.o knight.o bishop.o rook.o game.o player.o human.o computer.o strategy.o beginner.o move.o intermediate.o subject.o observer.o textobserver.o displayboard.o window.o graphicsobserver.o expert.o
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC} -lX11

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}

