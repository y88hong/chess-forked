#ifndef _MOVE_H_
#define _MOVE_H_

#include <memory>
#include <vector>

class ChessPiece;
class Spot;
class Board;

class Move
{
    std::shared_ptr<ChessPiece> pieceMoved;
    std::shared_ptr<Spot> start;
    std::shared_ptr<Spot> end;
    std::shared_ptr<ChessPiece> pieceKilled;
    std::shared_ptr<ChessPiece> piecePromote;

public:
    Move(std::shared_ptr<ChessPiece> piecemoved = nullptr, std::shared_ptr<Spot> start = nullptr,
        std::shared_ptr<Spot> end = nullptr, std::shared_ptr<ChessPiece> piecekilled = nullptr, std::shared_ptr<ChessPiece> piecepromote = nullptr);
    ~Move();
    std::shared_ptr<ChessPiece> getPieceMoved();
    std::shared_ptr<ChessPiece> getPieceKilled();
    bool isPawnMoveTwo();   // check if the pawn moves forward by two blocks, needed when identifying en passant
    bool isKingMoveTwoCol(); // check if the king moves horizontally by two blocks, this is needed for identifying casling move
    int getKingDirection(); // return 2 if the King moves to left, returns -2 if the King moves to right 
    int getDirection(); // return a positive number if the chesspiece moves downward, and negative if the chesspiece moves upward. needed when identifying the direction of en passant
    void movePiece(); // normal move
    void moveEnPassant(std::shared_ptr<Board> board); // make move for enpassant
    std::shared_ptr<Move> moveCastle(std::shared_ptr<Board> board); // make move for castling, moves rook and king on the board, return a move object of Rook (std::shared_ptr<Move> rookMove = kingMove->moveCastle(board)))
    void movePromote(); // move for promotion
    void undo(std::shared_ptr<Board> board); // undo one move
};

#endif

